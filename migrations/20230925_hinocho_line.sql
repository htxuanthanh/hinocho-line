/*
 Navicat MySQL Data Transfer

 Source Server         : Hinocho Local
 Source Server Type    : MySQL
 Source Server Version : 50738 (5.7.38)
 Source Host           : 127.0.0.1:4306
 Source Schema         : hinocho_line

 Target Server Type    : MySQL
 Target Server Version : 50738 (5.7.38)
 File Encoding         : 65001

 Date: 25/09/2023 21:54:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for authassignment
-- ----------------------------
DROP TABLE IF EXISTS `authassignment`;
CREATE TABLE `authassignment` (
  `itemname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userid` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bizrule` text COLLATE utf8mb4_unicode_ci,
  `data` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of authassignment
-- ----------------------------
BEGIN;
INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES ('SuperAdmin', '1', NULL, 'N;');
COMMIT;

-- ----------------------------
-- Table structure for authitem
-- ----------------------------
DROP TABLE IF EXISTS `authitem`;
CREATE TABLE `authitem` (
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `bizrule` text COLLATE utf8mb4_unicode_ci,
  `data` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of authitem
-- ----------------------------
BEGIN;
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('Admin', 2, 'Admin', NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('Cache.*', 1, 'Cache', NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('ExcelExport.*', 1, 'Export Excel', NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('QueryTool.*', 1, NULL, NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('ReadLog.*', 1, NULL, NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('Site.*', 1, NULL, NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('Site.Error', 0, NULL, NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('Site.Index', 0, NULL, NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('SuperAdmin', 2, 'SuperAdmin', '', 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('User.Admin.*', 1, 'Manage User', NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('User.Login.*', 1, 'Login', NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('User.Logout.*', 1, 'Logout', NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('User.Profile.*', 1, 'Profile', NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('User.ProfileField.*', 1, 'Profile Fields', NULL, 'N;');
INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES ('User.Recovery.*', 1, 'Recovery Password', NULL, 'N;');
COMMIT;

-- ----------------------------
-- Table structure for authitemchild
-- ----------------------------
DROP TABLE IF EXISTS `authitemchild`;
CREATE TABLE `authitemchild` (
  `parent` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of authitemchild
-- ----------------------------
BEGIN;
INSERT INTO `authitemchild` (`parent`, `child`) VALUES ('Admin', 'ExcelExport.*');
INSERT INTO `authitemchild` (`parent`, `child`) VALUES ('Admin', 'Site.*');
INSERT INTO `authitemchild` (`parent`, `child`) VALUES ('Admin', 'User.Admin.*');
INSERT INTO `authitemchild` (`parent`, `child`) VALUES ('Admin', 'User.Login.*');
INSERT INTO `authitemchild` (`parent`, `child`) VALUES ('Admin', 'User.Logout.*');
INSERT INTO `authitemchild` (`parent`, `child`) VALUES ('Admin', 'User.Profile.*');
INSERT INTO `authitemchild` (`parent`, `child`) VALUES ('Admin', 'User.ProfileField.*');
INSERT INTO `authitemchild` (`parent`, `child`) VALUES ('Admin', 'User.Recovery.*');
COMMIT;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `line_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i_id` (`id`),
  KEY `i_store_id` (`store_id`),
  KEY `i_line_id` (`line_id`),
  KEY `i_status` (`status`),
  KEY `i_deleted` (`deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of customer
-- ----------------------------
BEGIN;
INSERT INTO `customer` (`id`, `store_id`, `line_id`, `name`, `first_name`, `last_name`, `status`, `created_at`, `updated_at`, `deleted_at`, `deleted`) VALUES (1, 1, 'Uc5e9abb8c3e35e1513fe03037265471f', NULL, NULL, NULL, 1, '2023-09-20 21:13:13', '2023-09-20 21:13:13', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for import
-- ----------------------------
DROP TABLE IF EXISTS `import`;
CREATE TABLE `import` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `result` longtext COLLATE utf8mb4_unicode_ci,
  `size` bigint(20) DEFAULT NULL,
  `ext` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of import
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for profiles
-- ----------------------------
DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`) USING BTREE,
  CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of profiles
-- ----------------------------
BEGIN;
INSERT INTO `profiles` (`user_id`, `lastname`, `firstname`) VALUES (1, 'Admin', 'Administrator');
COMMIT;

-- ----------------------------
-- Table structure for profiles_fields
-- ----------------------------
DROP TABLE IF EXISTS `profiles_fields`;
CREATE TABLE `profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `varname` (`varname`,`widget`,`visible`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of profiles_fields
-- ----------------------------
BEGIN;
INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES (1, 'lastname', 'Last Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3);
INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES (2, 'firstname', 'First Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);
COMMIT;

-- ----------------------------
-- Table structure for rights
-- ----------------------------
DROP TABLE IF EXISTS `rights`;
CREATE TABLE `rights` (
  `itemname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`),
  CONSTRAINT `rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of rights
-- ----------------------------
BEGIN;
INSERT INTO `rights` (`itemname`, `type`, `weight`) VALUES ('Admin', 2, 1);
INSERT INTO `rights` (`itemname`, `type`, `weight`) VALUES ('SuperAdmin', 2, 0);
COMMIT;

-- ----------------------------
-- Table structure for store
-- ----------------------------
DROP TABLE IF EXISTS `store`;
CREATE TABLE `store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `line_bot_access_token` varchar(255) DEFAULT NULL,
  `line_bot_basic_id` varchar(255) DEFAULT NULL,
  `xid_client_id` varchar(255) DEFAULT NULL,
  `xid_client_secret` varchar(255) DEFAULT NULL,
  `xid_public_key` text,
  `xid_secret_key` text,
  `xid_auth_info` text,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i_id` (`id`) USING BTREE,
  KEY `i_status` (`status`) USING BTREE,
  KEY `i_deleted` (`deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of store
-- ----------------------------
BEGIN;
INSERT INTO `store` (`id`, `name`, `line_bot_access_token`, `line_bot_basic_id`, `xid_client_id`, `xid_client_secret`, `xid_public_key`, `xid_secret_key`, `xid_auth_info`, `status`, `created_at`, `updated_at`, `deleted_at`, `deleted`) VALUES (1, 'Store Test', 'HHcYMxN+EVfXEnQvDZRpznG2IVpSRb1Lb4QHo3EEJRCTLSQbIe6mYHrp5Geq4nJA48sSmgh1I72ewBbPFs/sb1dmHo8w7afId0Q6MmH1O9uPUovyQbWO7yd9RokxObybKCILogFX555eglwdQ+VzfAdB04t89/1O/w1cDnyilFU=', '@574mspyi', NULL, NULL, NULL, NULL, NULL, 1, '2023-09-20 19:02:39', '2023-09-20 19:02:41', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `avatar` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `store_id` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `email` (`email`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `superuser` (`superuser`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` (`id`, `username`, `password`, `email`, `activkey`, `create_at`, `lastvisit_at`, `superuser`, `status`, `avatar`, `phone`, `name`, `address`, `store_id`) VALUES (1, 'admin', 'c60138a73f760227b2f1fdb1fe9fa147', 'thanh.nguyen13@ntq-solution.com.vn', '374f705db69b5f537cb10f696852f7f4', '2019-06-20 10:55:09', '2019-09-04 08:18:55', 1, 1, NULL, NULL, 'Admin', NULL, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
