<?php

date_default_timezone_set('Asia/Tokyo');
ini_set('memory_limit', -1);

$config_path              = dirname(__FILE__) . "/config/config.ini";
$GLOBALS['config_common'] = parse_ini_file($config_path, TRUE);

// change the following paths if necessary
$frameworkPath = explode('framework', $GLOBALS['config_common']['project']['framework'])[0]. 'framework/';
$yiic=dirname(__FILE__).$frameworkPath . 'yiic.php';
$config=dirname(__FILE__).'/protected/config/console.php';

require_once($yiic);