<?php
/**
 * @var $this StoreController
 * @var $model WStore
 */
$this->breadcrumbs = array(
    Yii::t('web/menu', 'Manage Store') => array('index')
);
$labels = $model->attributeLabels();
$dataProvider = $model->search();
$row = 0;
?>

<div class="x_panel">
    <div class="x_title">

        <div class="pull-right">
            <?php echo CHtml::link(Yii::t('web/form', 'Create'), array('create'), array('class' => 'btn btn-success btn-sm')); ?>
        </div>

        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <?php $this->widget('booster.widgets.TbGridView', array(
            'id' => 'store-grid',
            'dataProvider' => $dataProvider,
            'filter' => $model,
            'enableSorting' => FALSE,
            'afterAjaxUpdate' => 'js:function(){initFreezeTable(\'store-grid\')}',
            'itemsCssClass' => 'table table-bordered table-striped table-hover jambo_table responsive-utilities table-freeze',
            'htmlOptions' => array('class' => 'grid-view table-freeze-grid grid-view-optimize'),
            'template' => '<div class="pager-inline">{pager}</div> {summary} {items} {pager}',
            'columns' => array(
                /*array(
                    'header' => Yii::t('web/label', 'column_count_label'),
                    'headerHtmlOptions' => array('style' => 'width: 40px;'),
                    'value' => function($data) use ($dataProvider, &$row){
                        ++$row;
                        $currentPage = $dataProvider->pagination->currentPage;
                        $pageSize = $dataProvider->pagination->pageSize;
                        return ($currentPage)*$pageSize + $row;
                    },
                ),*/
                array(
                    'name' => 'id',
                    'filter' => CHtml::activeTextField($model,'id', array(
                        'class' => 'form-control',
                        'onchange'=>'$("[name=\''.CHtml::activeName($model,'id').'\']").val(this.value)'
                    )),
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->id),array("store/update","id"=>$data->id))',

                ),
                array(
                    'name' => 'name',
                    'filter' => CHtml::activeTextField($model,'name', array(
                        'class' => 'form-control',
                        'onchange'=>'$("[name=\''.CHtml::activeName($model,'name').'\']").val(this.value)'
                    )),
                    'value' => '$data->name',
                ),
                array(
                    'name' => 'created_at',
                    'filter' => false,
                    'value' => '$data->created_at',
                ),
                array(
                    'class' => 'booster.widgets.TbButtonColumn',
                    'template' => '{update} {delete}',
                    'buttons' => array(
                        'update' => array(
                            'visible' => '$data->isEnableEdit()',
                            'options' => array('class' => 'update btn btn-sm btn-default'),
                        ),
                        'delete' => array(
                            'visible' => '$data->isEnableDelete()',
                            'options' => array('class' => 'delete btn btn-sm btn-danger'),
                        )
                    ),
                    'htmlOptions' => array('style' => 'min-width: 120px; text-align: center')
                ),
            )
        ));?>
    </div>
</div>

<script>
    $(document).ready(function () {
        initFreezeTable('store-grid');
    });
</script>