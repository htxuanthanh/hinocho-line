<?php
/**
 * @var $this StoreController
 * @var $model Wstore
 */
$this->breadcrumbs = array(
    Yii::t('web/menu','Manage Store') => array('index'),
    Yii::t('web/form','Create'),
);
?>

<div class="x_panel">

    <div class="x_content">
        <?php echo $this->renderPartial('_form', array('model' => $model))?>
    </div>

</div>
