<?php
/**
 * @var $this StoreController
 * @var $model Wstore
 * @var $form TbActiveForm
 */
?>

<div class="form">
<?php $form =$this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'store-form',
    'enableAjaxValidation' => true,
    'htmlOptions' => array(),
));?>

    <?php echo Yii::t('web/form','require_fields')?>
    <?php echo CHtml::errorSummary($model)?>

    <div class="form-group">
        <?php echo $form->labelEx($model,'name')?>
        <?php echo $form->textField($model,'name',array(
            'class'=>'form-control',
        ))?>
        <?php echo $form->error($model,'name')?>
    </div>

    <div class="ln_solid"></div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'line_bot_access_token')?>
        <?php echo $form->textField($model,'line_bot_access_token',array(
            'class'=>'form-control',
        ))?>
        <?php echo $form->error($model,'line_bot_access_token')?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'line_bot_basic_id')?>
        <?php echo $form->textField($model,'line_bot_basic_id',array(
            'class'=>'form-control',
        ))?>
        <?php echo $form->error($model,'line_bot_basic_id')?>
    </div>

    <div class="ln_solid"></div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'xid_client_id')?>
        <?php echo $form->textField($model,'xid_client_id',array(
            'class'=>'form-control',
        ))?>
        <?php echo $form->error($model,'xid_client_id')?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'xid_client_secret')?>
        <?php echo $form->textField($model,'xid_client_secret',array(
            'class'=>'form-control',
        ))?>
        <?php echo $form->error($model,'xid_client_secret')?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'xid_public_key')?>
        <?php echo $form->textArea($model,'xid_public_key',array(
            'class'=>'form-control',
        ))?>
        <?php echo $form->error($model,'xid_public_key')?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'xid_secret_key')?>
        <?php echo $form->textArea($model,'xid_secret_key',array(
            'class'=>'form-control',
        ))?>
        <?php echo $form->error($model,'xid_secret_key')?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'fureiru_uncheck_ver_url')?>
        <?php echo $form->textField($model,'fureiru_uncheck_ver_url',array(
            'class'=>'form-control',
        ))?>
        <?php echo $form->error($model,'fureiru_uncheck_ver_url')?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'fureiru_full_ver_url')?>
        <?php echo $form->textField($model,'fureiru_full_ver_url',array(
            'class'=>'form-control',
        ))?>
        <?php echo $form->error($model,'fureiru_full_ver_url')?>
    </div>

    <div class="ln_solid"></div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'timezone_url')?>
        <?php echo $form->textField($model,'timezone_url',array(
            'class'=>'form-control',
        ))?>
        <?php echo $form->error($model,'timezone_url')?>
    </div>

    <div class="ln_solid"></div>

    <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('web/form', 'Create') : Yii::t('web/form', 'Update'), array('class' => 'btn btn-success')); ?>

    <?php echo CHtml::link(Yii::t('web/form', 'Cancel'), Yii::app()->createUrl('store/index'), array('class' => 'btn btn-default')) ?>

<?php $this->endWidget(); ?>
</div>
