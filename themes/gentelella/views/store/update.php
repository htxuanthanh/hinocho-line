<?php
/**
 * @var $this StoreController
 * @var $model Wstore
 */
$this->breadcrumbs = array(
    Yii::t('web/menu','Manage Store') => array('admin'),
    Yii::t('web/form','Update'),
);
?>

<div class="x_panel">

    <div class="x_content">
        <?php echo $this->renderPartial('_form', array('model' => $model))?>
    </div>

</div>
