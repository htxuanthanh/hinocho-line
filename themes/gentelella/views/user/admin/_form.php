<?php
/**
 * @var $this AdminController
 * @var $model User
 * @var $form TbActiveForm
 */
?>

<div class="form">
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => TRUE,
    'htmlOptions' => array(),
))?>

    <?php echo Yii::t('web/form','require_fields')?>
    <?php echo CHtml::errorSummary($model)?>

    <div class="row">

        <div class="col-sm-3">
            <?php if ($model->isNewRecord){?>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'username')?>
                <?php echo $form->textField($model, 'username', array('class' => 'form-control'))?>
                <?php echo $form->error($model, 'username')?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model, 'password')?>
                <?php echo $form->passwordField($model, 'password', array('class' => 'form-control'))?>
                <?php echo $form->error($model, 'password')?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model, 're_password')?>
                <?php echo $form->passwordField($model, 're_password', array('class' => 'form-control'))?>
                <?php echo $form->error($model, 're_password')?>
            </div>
            <?php }else{ ?>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'username')?>
                <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'disabled' => true))?>
                <?php echo $form->error($model, 'username')?>
            </div>
            <?php } ?>
        </div>


        <div class="col-sm-3">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'name')?>
                <?php echo $form->textField($model, 'name', array('class' => 'form-control'))?>
                <?php echo $form->error($model, 'name')?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model, 'email')?>
                <?php echo $form->textField($model, 'email', array('class' => 'form-control'))?>
                <?php echo $form->error($model, 'email')?>
            </div>


            <div class="form-group">
                <?php echo $form->labelEx($model, 'phone')?>
                <?php echo $form->textField($model, 'phone', array('class' => 'form-control'))?>
                <?php echo $form->error($model, 'phone')?>
            </div>
        </div>

        <div class="col-sm-3">
            <?php if(empty(Yii::app()->user->store_id)){ ?>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'store_id')?>
                <?php echo $form->dropDownList($model, 'store_id', WStore::getListData(), array(
                    'class' => 'form-control',
                    'empty' => Yii::t('web/form', 'Select'),
                ))?>
                <?php echo $form->error($model, 'store_id')?>
            </div>
            <?php }?>

            <div class="form-group">
                <?php echo $form->labelEx($model, 'roles')?>
                <div>
                    <?php echo $form->checkBoxList($model,'roles',User::getListRoles(),array(
                        'class' => 'flat'
                    ))?>
                </div>
                <?php echo $form->error($model, 'roles')?>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'status')?>
                <div>
                    <?php echo $form->radioButtonList($model, 'status', User::getListStatus(), array('class' => 'flat'))?>
                </div>
                <?php echo $form->error($model, 'status')?>
            </div>
        </div>

    </div>

    <div class="ln_solid"></div>

    <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('web/form', 'Create') : Yii::t('web/form', 'Update'), array('class' => 'btn btn-success')); ?>

<?php $this->endWidget()?>
</div>