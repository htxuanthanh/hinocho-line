<?php
/**
* @var $model User
*/
$this->breadcrumbs = array(
    UserModule::t('Manage Users') => array('admin')
);
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?php echo UserModule::t("Manage Users"); ?></h2>

        <div class="pull-right">
            <?php echo CHtml::link(Yii::t('web/form', 'Create'), array('create'), array('class' => 'btn btn-success btn-sm')); ?>
        </div>

        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <?php
        $this->widget('booster.widgets.TbGridView', array(
            'dataProvider' => $model->search(),
            'filter' => $model,
            'enableSorting' => FALSE,
            'itemsCssClass' => 'table table-bordered table-striped table-hover jambo_table responsive-utilities',
            'columns' => array(
                array(
                    'header' => Yii::t('web/label', 'column_count_label'),
                    'headerHtmlOptions' => array('style' => 'width: 40px;'),
                    'value' => '++$row',
                ),
                array(
                    'name' => 'username',
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->username),array("admin/update","id"=>$data->id))',
                ),
                array(
                    'name' => 'name',
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->name)',
                ),
                array(
                    'name' => 'email',
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->email), "mailto:".$data->email)',
                ),
                array(
                    'name' => 'phone',
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->phone), "tel:".$data->phone)',
                ),
                array(
                    'name' => 'store_id',
                    'value' => function($data){
                        $value = !empty($data->store) ? ($data->store->id . ' - ' . $data->store->name) : '';
                        return $value;
                    },
                    'visible' => empty(Yii::app()->user->store_id),
                    'headerHtmlOptions' => array('style' => 'width:120px'),
                ),
                array(
                    'name' => 'roles',
                    'filter' => CHtml::activeDropDownList($model, 'roles',
                        User::getListRoles(),
                        array(
                            'class' => 'form-control',
                            'empty' => Yii::t('web/form', 'All'),
                        )
                    ),
                    'type' => 'raw',
                    'value' => function($data){
                        $return = null;
                        if(!empty($data->roles)){
                            $return='';
                            $first = true;
                            foreach ($data->roles as $role){
                                if(!$first){
                                    $return.= "<br/>";
                                }else{
                                    $first = false;
                                }
                                $return.= CHtml::encode(User::getRolesName($role));
                            }
                        }
                        return $return;
                    },
                    'htmlOptions' => array(
                        'nowrap' => 'nowrap'
                    )
                ),
                array(
                    'name' => 'status',
                    'filter' => CHtml::activeDropDownList($model, 'status',
                        User::getListStatus(),
                        array(
                            'class' => 'form-control',
                            'empty' => Yii::t('web/form', 'All'),
                        )
                    ),
                    'type' => 'raw',
                    'value' => function($data){

                        switch ($data->status){
                            case User::STATUS_ACTIVE:
                                $class = 'text-success';
                                break;
                            case User::STATUS_NOACTIVE:
                                $class = 'text-dark';
                                break;
                            case User::STATUS_BANNED:
                                $class = 'text-danger';
                                break;
                            default:
                                $class = '';
                        }

                        return "<span class='$class'>".User::itemAlias('UserStatus', $data->status)."</span>";
                    },
                    'headerHtmlOptions' => array(
                        'style' => 'width: 120px;',
                    ),
                    'htmlOptions' => array(
                        'style' => 'text-align: center;',
                    )
                ),
                array(
                    'class' => 'booster.widgets.TbButtonColumn',
                    'template' => '{update} {delete}',
                    'buttons' => array(
                        'delete' => array(
                            'visible' => 'Yii::app()->user->isSuperuser',
                        ),
                    ),
                    'htmlOptions' => array(
                        'class' => 'text-center',
                        'style' => 'width: 70px'
                    )
                ),
            ),
        ));

        ?>
    </div>
</div>
