<?php
$this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Profile");
$this->breadcrumbs = array(
    UserModule::t("Profile"),
);
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?php echo UserModule::t('Your profile'); ?></h2>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <?php $this->widget('booster.widgets.TbDetailView', array(
            'data' => $model,
            'htmlOptions' => array('class' => 'table-bordered detail-view'),
            'attributes' => array(
                'username',
                'email',
                'name',
                'phone',
                array(
                    'name' => 'roles',
                    'value' => function($data){
                        if(Yii::app()->user->isSuperUser){
                            return 'SuperAdmin';
                        }else{
                            $return = null;
                            if(!empty($data->roles)){
                                $return = '';
                                $first = true;
                                foreach ($data->roles as $role){
                                    if(!$first){
                                        $return.= "<br/>";
                                    }else{
                                        $first = false;
                                    }
                                    $return.= CHtml::encode(User::getRolesName($role));
                                }
                            }
                            return $return;
                        }
                    }
                ),
                array(
                    'name' => 'avatar',
                    'type' => 'raw',
                    'value' => function($data){
                        $value = null;
                        if(!empty($data->avatar)){
                            $url = Yii::app()->baseUrl . '/' . $data->avatar;
                            $value = CHtml::image($url, '', array('width' => '120'));
                        }
                        return $value;
                    }
                ),
                'create_at',
            )
        ));?>
    </div>
</div>




