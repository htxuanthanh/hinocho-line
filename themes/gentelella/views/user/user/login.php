

<div class="x_panel">
    <div class="x_title">
        <img src="<?php echo Yii::app()->theme->baseUrl?>/images/logo.svg" width="120">
    </div>

    <div class="x_content">
        <?php echo CHtml::beginForm(); ?>

        <h1><?php echo UserModule::t("Login"); ?></h1>

        <?php if(Yii::app()->user->hasFlash('loginMessage')): ?>
            <div class="success">
                <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
            </div>
        <?php endif; ?>

        <?php echo CHtml::errorSummary($model); ?>


        <div>
            <?php echo CHtml::activeTextField($model,'username', array(
                'class' => 'form-control',
                'placeholder' => UserModule::t("Username or Email"),
                'maxLength' => 20,
            )) ?>
        </div>
        <div>
            <?php echo CHtml::activePasswordField($model,'password', array(
                'class' => 'form-control',
                'placeholder' => UserModule::t("Password"),
                'maxLength' => 32,
            )) ?>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="rememberMe">
                    <?php echo CHtml::activeCheckBox($model,'rememberMe', array(
                        'class' => 'flat'
                    )); ?>
                    <label><?php echo CHtml::activeLabelEx($model,'rememberMe'); ?></label>
                </div>
            </div>

            <div class="col-xs-4 text-right">
                <?php echo CHtml::submitButton(UserModule::t("Login"), array(
                    'class' => 'btn btn-default submit'
                )); ?>
            </div>
        </div>

        <div>

        </div>
        <div>
            <?php echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl, array(
                'class' => 'reset_pass'
            )); ?>
        </div>

        <div class="clearfix"></div>

        <div class="separator">

            <?php $this->widget('ext.eauth.EAuthWidget', array(
                'action' => '/user/login',
                'popup' => false,
                'services' => array(
                    'google_oauth' => (object) array(
                        'id'            => 'google_oauth',
                        'class'         => 'GoogleOAuthService',
                        'client_id'     => $GLOBALS['config_common']['google']['client_id'],
                        'client_secret' => $GLOBALS['config_common']['google']['client_secret'],
                        'title'         => Yii::t('web/label', 'login_with_google'),
                        'htmlOptions'   => array(
                            'class' => 'btn btn-primary btn-xs'
                        )
                    ),
                ),
                'view' => 'custom_auth',
            )); ?>
        </div>

        <div class="separator">
            <div>
                <?php echo Yii::t('web/label','copyright');?>
            </div>
        </div>

        <?php echo CHtml::endForm(); ?>

    </div>
</div>

<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>

<script>
    $(document).ready(function() {
        if ($("input.flat")[0]) {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        }

        $('[type="submit"]').on('click', function () {
            $(this).addClass('disabled');
        });
    });
</script>
