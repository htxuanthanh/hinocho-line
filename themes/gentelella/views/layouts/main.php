<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="<?php echo Yii::app()->language?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo Yii::app()->theme->baseUrl?>/images/favico.ico" type="image/ico" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
    <!-- iCheck -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- Theme Css -->
    <link href="<?php echo Yii::app()->theme->baseUrl?>/css/theme.css?v=<?php echo APP_VERSION?>" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl?>/css/custom.css?v=<?php echo APP_VERSION?>" rel="stylesheet">

</head>

<body class="nav-md">

<div class="container body">
    <div class="main_container">

        <?php echo $this->renderPartial('//layouts/_left_col')?>

        <?php echo $this->renderPartial('//layouts/_top_nav')?>

        <!-- page content -->
        <div class="right_col" role="main">

            <?php if(isset($this->breadcrumbs)):?>
                <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links'=>$this->breadcrumbs,
                )); ?>
            <?php endif?>

            <?php $this->widget('booster.widgets.TbAlert'); ?>

            <?php echo $content; ?>
        </div>

        <?php echo $this->renderPartial('//layouts/_footer')?>

    </div>
</div>

<!-- FastClick -->
<script src="<?php echo Yii::app()->baseUrl?>/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo Yii::app()->baseUrl?>/vendors/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script src="<?php echo Yii::app()->baseUrl?>/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- iCheck -->
<script src="<?php echo Yii::app()->baseUrl?>/vendors/iCheck/icheck.min.js"></script>
<!-- Switchery -->
<script src="<?php echo Yii::app()->baseUrl?>/vendors/switchery/dist/switchery.min.js"></script>
<!-- FreezeTable -->
<script src="<?php echo Yii::app()->baseUrl?>/vendors/freeze-table/freeze-table.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo Yii::app()->theme->baseUrl?>/js/theme.js?v=<?php echo APP_VERSION?>"></script>
<script src="<?php echo Yii::app()->theme->baseUrl?>/js/custom.js?v=<?php echo APP_VERSION?>"></script>

</body>
</html>
