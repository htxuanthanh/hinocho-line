<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view menu_fixed">

        <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo $this->createUrl('/site/index') ?>" class="site_title">
                <span><?php echo CHtml::encode(Yii::app()->name) ?></span>
            </a>
        </div>

        <div class="clearfix"></div>

        <div class="ln_solid no-margin"></div>

        <?php if(!Yii::app()->user->isGuest){?>
        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <?php $this->widget('zii.widgets.CMenu', array(
                    'encodeLabel'   => FALSE,
                    'htmlOptions'   => array(
                        'class' => 'nav side-menu'
                    ),
                    'submenuHtmlOptions' => array(
                        'class' => 'nav child_menu',
                    ),
                    'items'         => array(
                        array(
                            'url'     => array('/store/index'),
                            'label'   => '<i class="fa fa-hospital-o"></i> ' . Yii::t('web/menu','Manage Store'),
                            'visible' => ((Yii::app()->user->checkAccess('Store.Index') || Yii::app()->user->checkAccess('Store.*'))),
                        ),
                    )
                ));?>
            </div>

            <div class="menu_section">
                <h3>Authentication</h3>
                <?php $this->widget('zii.widgets.CMenu', array(
                    'encodeLabel'   => FALSE,
                    'htmlOptions'   => array(
                        'class' => 'nav side-menu'
                    ),
                    'submenuHtmlOptions' => array(
                        'class' => 'nav child_menu',
                    ),
                    'items'         => array(
                        array(
                            'url'     => array('/user/admin'),
                            'label'   => '<i class="fa fa-users"></i> ' . Yii::app()->getModule('user')->t("Manage Users"),
                            'visible' => ((Yii::app()->user->checkAccess('User.Admin.Admin') || Yii::app()->user->checkAccess('User.Admin.*'))),
                        ),
                        array(
                            'url'     => 'javascript:;',
                            'label'   => '<i class="fa fa-user"></i> ' . Yii::app()->getModule('user')->t("Profile") . ' <span class="fa fa-chevron-down"></span>',
                            'visible' => !Yii::app()->user->isGuest,
                            'items'   => array(
                                array(
                                    'url'   => Yii::app()->getModule('user')->profileUrl,
                                    'label' => Yii::app()->getModule('user')->t("Profile"),
                                ),
                                array(
                                    'url'   => array('/user/profile/edit'),
                                    'label' => Yii::app()->getModule('user')->t("Edit"),
                                ),
                                array(
                                    'url'   => array('/user/profile/changepassword'),
                                    'label' => Yii::app()->getModule('user')->t("Change password"),
                                ),
                            ),
                        ),
                        array(
                            'url'     => 'javascript:;',
                            'label'   => '<i class="fa fa-shield"></i> ' . Rights::t('core', 'Assignments') . ' <span class="fa fa-chevron-down"></span>',
                            'visible' => Yii::app()->user->isSuperUser,
                            'items'   => array(
                                array(
                                    'label' => Rights::t('core', 'Assignments'),
                                    'url'   => array('/rights/assignment/view'),
                                ),
                                array(
                                    'label' => Rights::t('core', 'Permissions'),
                                    'url'   => array('/rights/authItem/permissions'),
                                ),
                                array(
                                    'label' => Rights::t('core', 'Roles'),
                                    'url'   => array('/rights/authItem/roles'),
                                ),
                                array(
                                    'label' => Rights::t('core', 'Tasks'),
                                    'url'   => array('/rights/authItem/tasks'),
                                ),
                                array(
                                    'label' => Rights::t('core', 'Operations'),
                                    'url'   => array('/rights/authItem/operations'),
                                ),
                                array(
                                    'label' => Rights::t('core', 'Generate'),
                                    'url'   => array('/rights/authItem/generate'),
                                ),
                            ),
                        ),
                        array(
                            'url'     => array('/cache/index'),
                            'label'   => '<i class="fa fa-eraser"></i> ' . Yii::t('web/cache', 'Cache'),
                            'visible' => Yii::app()->user->checkAccess('Cache.*'),
                        ),

                    )
                ));?>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo Yii::app()->createUrl('user/logout')?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->

        <?php } ?>
    </div>
</div>

