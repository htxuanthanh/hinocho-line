<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="<?php echo Yii::app()->language?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo Yii::app()->theme->baseUrl?>/images/favico.ico" type="image/ico" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Web Css -->
    <link href="<?php echo Yii::app()->theme->baseUrl?>/css/web.css?v=<?php echo APP_VERSION?>" rel="stylesheet">
</head>

<body>

<div id="main-content">
    <?php echo $content; ?>
</div>

<?php $this->renderPartial('//layouts/_loading'); ?>

<script src="<?php echo Yii::app()->theme->baseUrl?>/js/main.js?v=<?php echo APP_VERSION?>"></script>

</body>
</html>
