<div id="page_loading" style="display: none">
    <img src="<?php echo Yii::app()->theme->baseUrl?>/images/loading.gif"/>
</div>

<script>
    function showPageLoading()
    {
        $("#page_loading").show();
        setTimeout(function () { hidePageLoading() }, 3000);
    }

    function hidePageLoading()
    {
        $("#page_loading").hide();
    }
</script>


