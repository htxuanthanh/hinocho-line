<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - ' . Yii::t('web/label', 'Error');
$this->breadcrumbs=array(
	Yii::t('web/label', 'Error'),
);
?>

<div class="container-fluid">
    <div class="x_panel">
        <div class="x_title">
            <h2><?php echo Yii::t('web/label', 'Error') .' '. $code; ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="error">
                <p><?php echo Yii::t('web/label', 'Error Message'). ': ', CHtml::encode($message); ?></p>

                <?php if(Yii::app()->user->isSuperuser){
                    if(isset($file)){
                        echo "<p>".Yii::t('web/label', 'Error File'). ': '. CHtml::encode($file)."</p>";
                    }
                    if(isset($line)){
                        echo "<p>".Yii::t('web/label', 'Line'). ': '. CHtml::encode($line)."</p>";
                    }
                    if(isset($trace)){
                        echo "<pre>".$trace."</pre>";
                    }
                }?>
            </div>
        </div>
    </div>
</div>



