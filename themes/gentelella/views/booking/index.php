<iframe style="display:none" height="0" width="0" id="loader"></iframe>

<script>
(function(){

    var fallbackLink = 'http://example.com/my-web-app/'+window.location.search+window.location.hash;
    // Simple device detection
    var isiOS = navigator.userAgent.match('iPad') || navigator.userAgent.match('iPhone') || navigator.userAgent.match('iPod'),
        isAndroid = navigator.userAgent.match('Android');

    if (isiOS || isAndroid) {
        document.getElementById('loader').src = 'custom-protocol://my-app'+window.location.search+window.location.hash;
        fallbackLink = isAndroid ? 'https://play.google.com/store/apps/details?id=me.x.id' : 'itms-apps://itunes.apple.com/app/xid/id1495147544?mt=8' ;
    }
    window.setTimeout(function (){ window.location.replace(fallbackLink); }, 1);
})();
</script>