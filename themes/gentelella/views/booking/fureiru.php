<?php /**
 * @var $this WController
 * @var $botLink string
 * @var $token string
 * @var $store WStore
 */
?>

<div class="page_title">
    フレイル度チェックをしよう！
</div>

<div class="container">

    <div class="content-block">
        <p>
            説明文
            <br/>
            XidアプリをDLしてない方はDLしてね
        </p>
    </div>

    <div class="content-block">
        
    </div>

</div>

<div class="footer">
    <div id="menu_fix_bottom">
        <div>
            <a class="btn_booking" onclick="redirectToStore()" style="padding-top: 24px">
                xIDアプリをインストールする
            </a>
        </div>

        <div>
            <a class="btn_booking" onclick="authXid()">
                個人情報を登録して
                <br/>
                フレイル度チェック
            </a>
        </div>

        <div>
            <a class="btn_booking" href="<?php echo $store->fureiru_uncheck_ver_url?>">
                個人情報を登録せず
                <br/>
                フレイル度チェック
            </a>
        </div>

        <div>
            <a class="btn_back_to_bot" href="<?php echo $botLink?>">
                ◁トーク画面に戻る
            </a>
        </div>
    </div>

</div>

<script>
function authXid()
{
    showPageLoading();

    $.ajax({
        url : '/booking/authXid',
        type : 'POST',
        dataType: 'json',
        data : {
            YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken?>',
            mode: 'fureiru',
            token: '<?php echo $token?>',
        },
        success : function(response) {
            if (response.success) {
                window.location.href = response.redirect;
            }
        }
    });
}
</script>