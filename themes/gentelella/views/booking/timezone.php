<?php /**
 * @var $this WController
 * @var $botLink string
 * @var $token string
 * @var $store WStore
 */
?>

<div class="page_title">
    検診予約を行いましょう！
</div>

<div class="container">

    <div class="content-block">
        <p>
            説明文
            <br/>
            XidアプリをDLしてない方はDLしてね
        </p>
    </div>

    <div class="content-block">
        
    </div>

</div>

<div class="footer">
    <div id="menu_fix_bottom">
        <div>
            <a class="btn_booking" onclick="authXid()" style="padding-top: 24px">
                xIDアプリをインストールした方
            </a>
        </div>

        <div>
            <a class="btn_booking" onclick="redirectToStore()">
                xIDアプリをインストールしていない
                <br/>
                方はここにタップしてインストールする
            </a>
        </div>

        <div>
            <a class="btn_back_to_bot" href="<?php echo $botLink?>">
                ◁トーク画面に戻る
            </a>
        </div>
    </div>

</div>

<script>
function authXid()
{
    showPageLoading();

    $.ajax({
        url : '/booking/authXid',
        type : 'POST',
        dataType: 'json',
        data : {
            YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken?>',
            mode: 'timezone',
            token: '<?php echo $token?>',
        },
        success : function(response) {
            if (response.success) {
                window.location.href = response.redirect;
            }
        }
    });
}
</script>