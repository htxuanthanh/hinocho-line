<?php

ini_set("session.cookie_httponly", "true");
date_default_timezone_set('Asia/Tokyo');

// All(prject name,db connection...etc) config should be in "config/config.ini"
$config_path              = dirname(__FILE__) . "/config/config.ini";
$GLOBALS['config_common'] = parse_ini_file($config_path, TRUE);

$yii    = dirname(__FILE__) . $GLOBALS['config_common']['project']['framework'];
$config = dirname(__FILE__) . '/protected/web/config/main.php';

error_reporting($GLOBALS['config_common']['debug_mode']['display_errors']);
defined('YII_DEBUG') or define('YII_DEBUG', $GLOBALS['config_common']['debug_mode']['state']);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

include_once ("config/const.php");

if (!function_exists('w3_array_union_recursive')) {
    /**
     * This function does similar work to $array1+$array2,
     * except that this union is applied recursively.
     *
     * @param array $array1 - more important array
     * @param array $array2 - values of this array get overwritten
     *
     * @return array
     */
    function w3_array_union_recursive($array1, $array2)
    {
        $retval = $array1 + $array2;
        foreach ($array1 as $key => $value) {
            if (isset($array2[$key]) && isset($array2[$key]) && is_array($array1[$key]) && is_array($array2[$key])) {
                $retval[$key] = w3_array_union_recursive($array1[$key], $array2[$key]);
            }
        }
        return $retval;
    }
}

if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
        $headers = array ();
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

if (!function_exists('dd')) {
    function dd($var) {
        CVarDumper::dump($var,10,true);
        die();
    }
}

require_once($yii);
Yii::createWebApplication($config)->run();
