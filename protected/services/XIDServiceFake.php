<?php

class XIDServiceFake extends XIDService
{

    public function token($client_id, $client_secret, $code, $redirect_uri)
    {
        return json_encode([
            "access_token" => "dq2tO16OayqZAAlVC...cqKuyiwMly3RHCuf9hjq4",
            "id_token" => "eyJhbGciOiJSUzI1NiIsIm...Dgch06BGykfpkELF1UGNVg",
            "expires_in" => "604799",
            "scope" => "openid verification signing",
            "token_type" => "bearer"
        ]);
    }

    public function userData($access_token)
    {
        return json_encode([
            "last_name" => "公的",
            "first_name" => "花子",
            "previous_name" => "6DGneWk0FEzrR+xJeQlUEfJFg4Joc...",
            "year" => "Yk3WBSWA5ik++rpSpREDlK5U5...",
            "month" => "bMRyL9x1Wx1IovYoYNySf4RCm...",
            "date" => "MKWCLHfCiXODiRa4JXpH14yGF...",
            "prefecture" => "jIVZHGXG2al02gGixILKrKh4kBcG...",
            "city" => "AiNsiiGpmf0JkwoHf/bNLzueH/e32...",
            "address" => "TEQx5r9PIiv0pLgec1c+sIHtsW6o+...",
            "sub_char_common_name" => "Ov/JstVuddd2gCy7pKPTEQx58jA...",
            "sub_char_previous_name" => "6DGneWk0FEzrR+xJeQlUEfJFg4J...",
            "sub_char_address" => "Ov/JstVuddd2gCy7pKPTEQx58jA...",
            "gender" => "kuplFsYGysAdS535a7MtNEKLZA...",
            "verified_at" => "1637483655"
        ]);
    }

    public function decryptUserData($userData, $publicKey, $privateKey)
    {
        return [
            "last_name" => "公的",
            "first_name" => "花子",
            "previous_name" => "",
            "year" => "",
            "month" => "",
            "date" => "",
            "prefecture" => "",
            "city" => "",
            "address" => "",
            "sub_char_common_name" => "",
            "sub_char_previous_name" => "",
            "sub_char_address" => "",
            "gender" => "",
            "verified_at" => "1637483655"
        ];
    }

    public function myNumberRequest($access_token, $params)
    {
        return json_encode([
            "request_id" => "ilOAn2yR9T0IgRgxX...Y4k0ZEwyV9-vlkmOmp01NzoRST1"
        ]);
    }

    public function myNumberStatus($access_token, $request_id)
    {
        return json_encode([
            "status" => "fulfilled",
            "mynumber" => "bMRyL9x1Wx1IovYoYNySf4RCm…",
            "last_name" => "Ov/JstVuddd2gCy7pKPTEQx58jA...",
            "first_name" => "m4eAM9Cw8Wdwoo/vvdqycDikh2...",
            "previous_name" => "6DGneWk0FEzrR+xJeQlUEfJFg4Joc...",
            "gender" => "kuplFsYGysAdS535a7MtNEKLZA...",
            "year" => "Yk3WBSWA5ik++rpSpREDlK5U5...",
            "month" => "bMRyL9x1Wx1IovYoYNySf4RCm...",
            "date" => "MKWCLHfCiXODiRa4JXpH14yGF...",
            "prefecture" => "jIVZHGXG2al02gGixILKrKh4kBcG...",
            "city" => "AiNsiiGpmf0JkwoHf/bNLzueH/e32...",
            "address" => "TEQx5r9PIiv0pLgec1c+sIHtsW6o+...",
            "verified_at" => "1639985738"
        ]);
    }

    public function decryptMyNumberStatus($myNumberStatus, $publicKey, $privateKey)
    {
        return [
            "status" => "fulfilled",
            "mynumber" => "1234567890",
            "last_name" => "",
            "first_name" => "",
            "previous_name" => "",
            "gender" => "",
            "year" => "",
            "month" => "",
            "date" => "",
            "prefecture" => "",
            "city" => "",
            "address" => "",
            "verified_at" => ""
        ];
    }

}