<?php

class LineService
{
    CONST BASIC_URI_LINE_INVITE = 'https://line.me/R/ti/p/';


    public static function getBotLinkByStore($storeId)
    {
        $store = WStore::model()->findByPk($storeId);
        if (!empty($store) && !empty($store->line_bot_basic_id)) {
            return self::BASIC_URI_LINE_INVITE . $store->line_bot_basic_id;
        }
        return FALSE;
    }



}