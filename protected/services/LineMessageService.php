<?php

class LineMessageService
{
    CONST LINE_DOMAIN =  [
        'BOT_REPLY_V2' => 'https://api.line.me/v2/bot/message/reply',
        'BOT_MESSAGE' => 'https://api.line.me/v2/bot/message/',
        'BOT_PUSH_MESSAGE' => 'https://api.line.me/v2/bot/message/push'
    ];

    CONST MESSAGES = [
        'fureiru' => 'フレイル',
        'timezone' => '健診予約',
    ];

    protected $baseUrl;

    protected $accessToken;
    protected $botId;
    protected $storeId;
    protected $customerId;

    protected $customerJWT;

    public function init($params)
    {
        $this->baseUrl = $GLOBALS['config_common']['project']['hostname'];

        if (!empty($params['access_token'])) {
            $this->accessToken = $params['access_token'];
        }
        if (!empty($params['bot_id'])) {
            $this->botId = $params['bot_id'];
        }
        if (!empty($params['store_id'])) {
            $this->storeId = $params['store_id'];
        }
    }

    public function makeMessageReply($messageReceive)
    {
        $this->checkCustomer($messageReceive);

        $messageReply = [];
        if (empty($messageReceive['type'])) {
            return false;
        }

        if ($messageReceive['type'] == 'message') {
            switch ($messageReceive['message']['text']) {
                case self::MESSAGES['fureiru']:
                    $messageReply = $this->makeMessageFureiru($messageReceive);
                    break;
                case self::MESSAGES['timezone']:
                    $messageReply = $this->makeMessageTimezone($messageReceive);
            }
        }

        if ($messageReceive['type'] == 'postback') {

            file_put_contents('/var/www/logs/log.txt', json_encode($messageReceive)."\n", FILE_APPEND);
        }

        return $messageReply;
    }

    public function pushMessageReply($messageData)
    {
        $ch = curl_init(self::LINE_DOMAIN['BOT_REPLY_V2']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($messageData));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if (!empty($this->botId)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json; charser=UTF-8',
                'Authorization: Bearer ' . $this->accessToken,
                'X-Line-Bot-Id:' . $this->botId
            ]);
        } else {
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json; charser=UTF-8',
                'Authorization: Bearer ' . $this->accessToken
            ]);
        }

        $result = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        return [
            'response' => $result,
            'error' => $err
        ];
    }

    public function pushMessage($messageData)
    {
        $ch = curl_init(self::LINE_DOMAIN['BOT_PUSH_MESSAGE']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($messageData));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if (!empty($this->botId)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json; charser=UTF-8',
                'Authorization: Bearer ' . $this->accessToken,
                'X-Line-Bot-Id:' . $this->botId
            ]);
        } else {
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json; charser=UTF-8',
                'Authorization: Bearer ' . $this->accessToken
            ]);
        }

        $result = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        return [
            'response' => $result,
            'error' => $err
        ];
    }

    private function checkCustomer($messageReceive)
    {
        if (!empty($messageReceive['source']) && $messageReceive['source']['type'] == 'user') {
            $lineId = $messageReceive['source']['userId'];

            $criteria = new CDbCriteria();
            $criteria->compare('line_id', $lineId);
            $customer = WCustomer::model()->find($criteria);
            if (empty($customer)) {
                $customer = new WCustomer();
                $customer->line_id = $lineId;
                $customer->store_id = $this->storeId;
                $customer->save();
            }
            $this->customerId = $customer->id;
        }
        $this->generateCustomerJWT(
            [
                'customer_id' => $customer->id,
                'line_id' => $customer->line_id,
                'store_id' => $customer->store_id,
            ]
        );
    }

    private function generateCustomerJWT($data)
    {
        $this->customerJWT = Yii::app()->JWT->encode($data);
    }

    private function makeMessageFureiru($messageReceive)
    {
        $url = $this->baseUrl.'/booking/fureiru' . '?token=' . $this->customerJWT;

        if (!empty($this->customerId)){
            $customer = WCustomer::model()->findByPk($this->customerId);
            if (!empty($customer) && !empty($customer->xid_info)) {
                $store = WStore::model()->findByPk($this->storeId);
                if (!empty($store) && !empty($store->fureiru_full_ver_url)) {
                    $url = $store->fureiru_full_ver_url;
                }
            }
        }

        $message = [
            "replyToken" => $messageReceive['replyToken'],
            "messages" => [
                [
                    'type' => 'template',
                    'altText' => 'フレイル',
                    'template' => [
                        'type' => 'buttons',
                        'text' => "下のボタンをクリックして予約に進みます。",
                        'actions' => [
                            [
                                "type" => "uri",
                                "label" => "フレイル",
                                "uri"   => $url,
                            ],
                        ]
                    ]
                ]
            ]
        ];

        return $message;
    }

    private function makeMessageTimezone($messageReceive)
    {
        $action = [
            [
                "type" => "uri",
                "label" => "健診予約",
                "uri"   => $this->baseUrl.'/booking/timezone' . '?token=' . $this->customerJWT
            ]
        ];

        if (!empty($this->customerId)){
            $customer = WCustomer::model()->findByPk($this->customerId);
            if (!empty($customer) && !empty($customer->xid_info)) {
                $store = WStore::model()->findByPk($this->storeId);
                if (!empty($store) && !empty($store->timezone_url)) {
                    $action = [
                        [
                            "type" => "uri",
                            "label" => "集団",
                            "uri"   => $store->timezone_url
                        ],
                        [
                            "type" => "postback",
                            "label" => "個別",
                            "data" => "booking_time_zone_personal",
                        ],
                    ];
                }
            }
        }


        $message = [
            "replyToken" => $messageReceive['replyToken'],
            "messages" => [
                [
                    'type' => 'template',
                    'altText' => '健診予約',
                    'template' => [
                        'type' => 'buttons',
                        'text' => "下のボタンをクリックして予約に進みます。",
                        'actions' => $action
                    ]
                ]
            ]
        ];

        return $message;
    }

    /**
     * @param $store WStore
     * @param $customer WCustomer
     * @return array
     */
    public function makeMessageBookingTimezone($store, $customer)
    {
        $message = [
            "to"       => $customer->line_id,
            "messages" => [
                [
                    'type' => 'template',
                    'altText' => '健診予約',
                    'template' => [
                        'type' => 'buttons',
                        'text' => "受診方法（集団・個別）を選択してください。",
                        'actions' => [
                            [
                                "type" => "uri",
                                "label" => "集団",
                                "uri"   => $store->timezone_url
                            ],
                            [
                                "type" => "postback",
                                "label" => "個別",
                                "data" => "booking_time_zone_personal",
                            ],
                        ]
                    ]
                ],
            ]
        ];

        return $message;
    }

}