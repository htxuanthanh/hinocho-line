<?php

class XIDService
{
    protected $authUrl;
    protected $apiUrl;

    public $state;
    public $nonce;

    public $userDataNoEncryptedField = [
        'last_name',
        'first_name',
        'verified_at'
    ];

    public $myNumberStatusNoEncryptedField = [
        'status',
        'verified_at'
    ];

    public function __construct()
    {
        $this->authUrl = $GLOBALS['config_common']['xid']['auth_url'];
        $this->apiUrl = $GLOBALS['config_common']['xid']['api_url'];

    }

    public function auth($client_id, $redirect_uri)
    {
        $header = [
            'Accept: application/json'
        ];
        $params = [
            'client_id'     => $client_id,
            'scope'         => 'openid',
            'redirect_uri'  => $redirect_uri,
            'response_type' => 'code',
            'response_mode' => 'query',
            'state'         => $this->state = Utils::randomString(8, RANDOM_NUMBER),
            'nonce'         => $this->nonce = strtolower(Utils::randomString(10, RANDOM_TEXT)),
        ];

        return Utils::callAPI('GET', $this->authUrl.'/auth', $header, $params);
    }

    public function token($client_id, $client_secret, $code, $redirect_uri)
    {
        $header = [
            'Accept: application/json',
            'Content-Type: application/x-www-form-urlencoded',
            'Basic ' . base64_encode("{$client_id}:{$client_secret}"),
        ];
        $params = [
            'code'          => $code,
            'grant_type'    => 'authorization_code',
            'client_id'     => $client_id,
            'redirect_uri'  => $redirect_uri,
        ];

        return Utils::callAPI('POST', $this->authUrl.'/token', $header, $params);
    }

    public function userData($access_token)
    {
        $header = [
            'Accept: application/json',
            'Authorization: Bearer ' . $access_token,
        ];
        $params = [];

        return Utils::callAPI('GET', $this->apiUrl.'/verification/userdata', $header, $params);
    }

    public function decryptUserData($userData, $publicKey, $privateKey)
    {
        $decryptedData = [];
        $jsonData = json_decode($userData, true);

        $privateKeyDecode = base64_decode($privateKey);
        $publicKeyDecode = base64_decode($publicKey);

        foreach ($jsonData as $key => $value)
        {
            if (in_array($key, $this->userDataNoEncryptedField)) {
                $decryptedValue = $value;
            } else {
                $cipher = base64_decode($value);
                $keyPair = sodium_crypto_box_keypair_from_secretkey_and_publickey($privateKeyDecode, $publicKeyDecode);
                $decryptedValue = sodium_crypto_box_seal_open(substr($cipher, 24), $keyPair);
            }
            $decryptedData[$key] = $decryptedValue;
        }

        return $decryptedData;
    }

    public function myNumberRequest($access_token, $params)
    {
        $header = [
            'Accept: application/json',
            'Authorization: Bearer ' . $access_token,
            'Content-Type: application/json',
        ];

        $data = json_encode([
            'reason' => !empty($params['reason']) ? $params['reason'] : '',
            'notification_title' => !empty($params['notification_title']) ? $params['notification_title'] : '',
            'notification_description' => !empty($params['notification_description']) ? $params['notification_description'] : '',
        ]);

        return Utils::callAPIWithJsonData('POST', $this->apiUrl.'/mynumber/request', $header, $data);
    }

    public function myNumberStatus($access_token, $request_id)
    {
        $header = [
            'Accept: application/json',
            'Authorization: Bearer ' . $access_token,
        ];

        $params = ['request_id' => $request_id];

        return Utils::callAPI('GET', $this->apiUrl.'/mynumber/request', $header, $params);
    }

    public function decryptMyNumberStatus($myNumberStatus, $publicKey, $privateKey)
    {
        $decryptedData = [];
        $jsonData = json_decode($myNumberStatus, true);

        $privateKeyDecode = base64_decode($privateKey);
        $publicKeyDecode = base64_decode($publicKey);

        foreach ($jsonData as $key => $value)
        {
            if (in_array($key, $this->myNumberStatusNoEncryptedField)) {
                $decryptedValue = $value;
            } else {
                $cipher = base64_decode($value);
                $keyPair = sodium_crypto_box_keypair_from_secretkey_and_publickey($privateKeyDecode, $publicKeyDecode);
                $decryptedValue = sodium_crypto_box_seal_open(substr($cipher, 24), $keyPair);
            }
            $decryptedData[$key] = $decryptedValue;
        }

        return $decryptedData;
    }

}