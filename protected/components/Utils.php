<?php

class Utils
{
    //send mail
    public static function sendEmail($from, $to, $subject, $content = '', $views_layout_path = 'webroot.themes.gentelella.views.layouts')
    {
        $mail = new YiiMailer();
        $mail->setLayoutPath($views_layout_path);
        $mail->setData(array('message' => $content, 'name' => $from));
        $mail->setFrom(Yii::app()->params->adminEmail['username'], $from);
        $mail->setTo($to);
        $mail->setSubject($from . ' | ' . $subject);
        $mail->setSmtp(Yii::app()->params->adminEmail['host'], Yii::app()->params->adminEmail['port'], Yii::app()->params->adminEmail['secure'], TRUE, Yii::app()->params->adminEmail['username'], Yii::app()->params->adminEmail['password']);
        if ($mail->send()) {// echo 'Email was sent';

        } else {
            CVarDumper::dump($mail->getError(), 10, TRUE);
        }
    }

    //CSV
    public static function exportCSV($file_name, $data)
    {
        header("Content-Type:text/csv, charset=utf-8"); // Config header utf-8.
        header("Content-Disposition:attachment;filename=$file_name.csv");
        $output = fopen("php://output", 'w') or die("Can't open php://output");
        fputs($output, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF))); // Config input utf-8

        foreach ($data as $line) {
            //Input each row in csv.
            fputcsv($output, $line);
        }
        fclose($output) or die("Can't close php://output");
    }

    //XLS
    public static function exportExcel($file_name, $dataTableHtml, $sheetName = null)
    {
        if(!$sheetName){$sheetName='Sheet 1';}
        header('Content-type: application/excel');
        header('Content-Disposition: attachment; filename='.$file_name);
        $output = '<html xmlns:x="urn:schemas-microsoft-com:office:excel"><head><!--[if gte mso 9]>
            <xml>
                <x:ExcelWorkbook>
                    <x:ExcelWorksheets>
                        <x:ExcelWorksheet>
                            <x:Name>'.$sheetName.'</x:Name>
                            <x:WorksheetOptions>
                                <x:Print>
                                    <x:ValidPrinterInfo/>
                                </x:Print>
                            </x:WorksheetOptions>
                        </x:ExcelWorksheet>
                    </x:ExcelWorksheets>
                </x:ExcelWorkbook>
            </xml>
            <![endif]--></head><body>';
        $output.= $dataTableHtml;
        $output.='</body></html>';
        echo $output;
    }

    // curl
    public static function callAPI($method, $url, $header = array(), $data = array(), &$http_status = 200){
        $curl = curl_init();

        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }

    public static function callAPIWithJsonData($method, $url, $header = array(), $data = [], &$http_status = 200){
        $curl = curl_init();

        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                break;
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }

    public static function getDayOfWeek($date = null)
    {
        if(empty($date)) $date = date('Y-m-d');
        return date('w', strtotime($date));
    }

    public static function getDayOfWeekName($date = null)
    {
        if(empty($date)) $date = date('Y-m-d');
        return date('l', strtotime($date));
    }

    public static function getDayOfWeekShortName($date = null){
        if(empty($date)) $date = date('Y-m-d');
        return strtolower(substr(date('l', strtotime($date)),0,3));
    }

    public static function getListDateInRange($start_date, $end_date)
    {
        $arr_date = array();
        $start_date = date('Y-m-d', strtotime(str_replace('/','-',$start_date))) . ' 00:00:00';
        $end_date = date('Y-m-d', strtotime(str_replace('/','-',$end_date))) . ' 23:59:59';
        $period = new DatePeriod(
            new DateTime($start_date),
            new DateInterval('P1D'),
            new DateTime($end_date)
        );
        foreach ($period as $key => $value){
            $arr_date[] = $value->format('Y-m-d');
        }
        return $arr_date;
    }

    public static function convertExcelTimeToPHP($excel_time)
    {
        if(is_numeric($excel_time)){
            $total = $excel_time * 24; //multiply by the 24 hours
            $hour = floor($total); //Gets the natural number part
            $minute_fraction = $total - $hour; //Now has only the decimal part
            $minute = round($minute_fraction * 60); //Get the number of minutes
            $php_time = (($hour >= 10) ? $hour : '0'.$hour) . ':' . (($minute >= 10) ? $minute : '0'.$minute);
        }else{
            $php_time = $excel_time;
        }
        return $php_time;
    }

    public static function convertExcelDateToPHP($excel_date, $format = 'Y-m-d')
    {
        if(is_numeric($excel_date)){
            $php_date = date($format, PHPExcel_Shared_Date::ExcelToPHP($excel_date));
        }else{
            $php_date = date($format, strtotime(str_replace('/','-', $excel_date)));
        }
        return $php_date;
    }

    public static function utf8convert($str, $keep_upper_char = FALSE)
    {
        if(!$str) return false;
        if($keep_upper_char){
            $utf8 = array(
                'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
                'd'=>'đ',
                'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
                'i'=>'í|ì|ỉ|ĩ|ị',
                'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
                'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
                'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
                'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
                'D'=>'Đ',
                'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
                'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
                'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
                'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
                'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
            );
        }else{
            $utf8 = array(
                'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
                'd'=>'đ|Đ',
                'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
                'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',
                'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
                'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
                'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
            );
        }


        foreach($utf8 as $ascii=>$uni) $str = preg_replace("/($uni)/i",$ascii,$str);

        return $str;
    }


    public static function unsign_string($str, $separator = '-', $keep_special_chars = FALSE)
    {
        $str = str_replace(array("à", "á", "ạ", "ả", "ã", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ"), "a", $str);
        $str = str_replace(array("À", "Á", "Ạ", "Ả", "Ã", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ"), "A", $str);
        $str = str_replace(array("è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ"), "e", $str);
        $str = str_replace(array("È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ"), "E", $str);
        $str = str_replace("đ", "d", $str);
        $str = str_replace("Đ", "D", $str);
        $str = str_replace(array("ỳ", "ý", "ỵ", "ỷ", "ỹ", "ỹ"), "y", $str);
        $str = str_replace(array("Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ"), "Y", $str);
        $str = str_replace(array("ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ"), "u", $str);
        $str = str_replace(array("Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ"), "U", $str);
        $str = str_replace(array("ì", "í", "ị", "ỉ", "ĩ"), "i", $str);
        $str = str_replace(array("Ì", "Í", "Ị", "Ỉ", "Ĩ"), "I", $str);
        $str = str_replace(array("ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ"), "o", $str);
        $str = str_replace(array("Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ"), "O", $str);
        if ($keep_special_chars == FALSE) {
            $str = str_replace(array('–', '…', '“', '”', "~", "!", "@", "#", "$", "%", "^", "&", "*", "/", "\\", "?", "<", ">", "'", "\"", ":", ";", "{", "}", "[", "]", "|", "(", ")", ",", ".", "`", "+", "=", "-"), $separator, $str);
            $str = preg_replace("/[^_A-Za-z0-9- ]/i", '', $str);
        }

        $str = str_replace(' ', $separator, $str);

        return trim(strtolower($str), "-");
    }

    public static function ultimate_trim($str)
    {
        return trim(preg_replace("/\s+/", " ", $str));
    }

    public static function averageTime($list_time)
    {
        return (!empty($list_time)) ? (date('H:i:s', array_sum(array_map('strtotime', $list_time)) / count($list_time))) : '';
    }

    public static function addTimeZoneToDate($date, $timeZone= 'GMT+0900')
    {
        return date("Y-m-d H:i:s", strtotime(str_replace('/','-',$date))) . ' ' . $timeZone;
    }

    public static function getExcelColumnNameByIndex($index)
    {
        $numeric = ($index - 1) % 26;
        $letter = chr(65 + $numeric);
        $subtract_index = intval(($index - 1) / 26);
        if ($subtract_index > 0) {
            return self::getExcelColumnNameByIndex($subtract_index) . $letter;
        } else {
            return $letter;
        }
    }

    public static function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public static function randomString($length = 10, $mode = RANDOM_ALL)
    {
        switch ($mode) {
            case RANDOM_TEXT:
                $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case RANDOM_NUMBER:
                $characters = '0123456789';
                break;
            default:
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
