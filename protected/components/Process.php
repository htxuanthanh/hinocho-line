<?php

class Process{

    public $pid;
    public $command;

    protected $os;

    CONST OS_WINDOWS = 'WINDOWS';
    CONST OS_LINUX = 'LINUX';
    CONST OS_OTHER = 'OTHER';

    public function __construct()
    {
        if(strtoupper(PHP_OS) == self::OS_WINDOWS){
            $this->os = self::OS_WINDOWS;
        }else if(strtoupper(PHP_OS) == self::OS_LINUX){
            $this->os = self::OS_LINUX;
        }else{
            $this->os = self::OS_OTHER;
        }
    }


    public function start()
    {
        if(!empty($this->command)){
            $descriptorspec = [
                0 => ['pipe', 'r'],
                1 => ['pipe', 'w'],
                2 => ['pipe', 'w']
            ];
            $proc = proc_open($this->command, $descriptorspec, $pipes);
            if (is_resource($proc)) {
                proc_close($proc);
            }
            if($proc !== FALSE){
                $proc_details = proc_get_status($proc);
                $this->pid = $proc_details['pid'];
                return TRUE;
            }else{
                return FALSE;
            }
        }
        return FALSE;
    }

    public function stop()
    {
        if($this->isRunning()){
            if($this->os == self::OS_WINDOWS){
                $result = exec("taskkill /F /PID $this->pid", $output);
                if($result !== FALSE){
                    return TRUE;
                }else{
                    return FALSE;
                }
            }else if($this->os == self::OS_LINUX){
                $result = exec("kill -9 $this->pid", $output);
                if($result !== FALSE){
                    return TRUE;
                }else{
                    return FALSE;
                }
            }else{
                return 'OS NOT SUPPORTED';
            }
        }else{
            return TRUE;
        }
    }

    public function isRunning()
    {
        $flag = FALSE;
        if(!empty($this->pid)){
            if($this->os == self::OS_WINDOWS){
                $processes = explode( "\n", shell_exec( "tasklist.exe" ));
                foreach($processes as $process){
                    if(empty($process) || strpos( "Image Name", $process ) === 0 || strpos( "===", $process ) === 0) continue;
                    if(preg_match('/^(.*)\s+'.$this->pid.'/', $process)) {
                        $flag = true;
                        break;
                    }
                }
            }else if($this->os == self::OS_LINUX){
                if (file_exists("/proc/$this->pid")){
                    $flag = true;
                }
            }
        }
        return $flag;
    }

}
