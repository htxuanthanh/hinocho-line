<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';
	public $layout = '//layouts/login';

	protected $maximum_login_attempts = 3;
	protected $loginTimeOut = 60;

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
        $loginDenyTimeout = Yii::app()->user->getState('login_deny_timeout');
        $loginAttempts = Yii::app()->user->getState('login_attempts');
        if(empty($loginAttempts)){
            $loginAttempts = 1;
        }

        if(empty($loginDenyTimeout) && $loginAttempts > $this->maximum_login_attempts){
            $loginDenyTimeout = time();
            Yii::app()->user->setState('login_deny_timeout', $loginDenyTimeout);
        }

        $now = time();
        if($loginDenyTimeout && ($now - $loginDenyTimeout > $this->loginTimeOut)){
            $loginAttempts = 1;
            Yii::app()->user->setState('login_attempts', $loginAttempts);
            Yii::app()->user->setState('login_deny_timeout', null);
        }

		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
            if($loginAttempts > $this->maximum_login_attempts){
                $model->addError('username', 'MAXIMUM LOGIN ATTEMPTS');
            }else{
                if(isset($_POST['UserLogin']))
                {
                    $model->attributes=$_POST['UserLogin'];
                    // validate user input and redirect to previous page if valid
                    if($model->validate()) {
                        $this->lastVisit();
                        $this->setState();

                        $this->redirect(Yii::app()->controller->module->returnUrl);
                    }else{
                        $loginAttempts++;
                        Yii::app()->user->setState('login_attempts', $loginAttempts);
                    }
                }else{
                    $this->googleOAuthLogin($model);
                }
            }

			// display the login form
			$this->render('/user/login',array('model'=>$model));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}

	private function lastVisit() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit_at = date('Y-m-d H:i:s');
		$lastVisit->save();
	}

	private function googleOAuthLogin(UserLogin &$model){
        $serviceName = Yii::app()->request->getQuery('service');
        if (isset($serviceName) && $serviceName == 'google_oauth') {
            /** @var $eauth EAuthServiceBase */
            $eauth = Yii::app()->eauth->getIdentity($serviceName);
            $eauth->redirectUrl = Yii::app()->controller->module->returnUrl;
            $eauth->cancelUrl = $this->createAbsoluteUrl('/user/login');

            try {
                if ($eauth->authenticate()) {
                    //var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes());
                    $identity = new GoogleEAuthUserIdentity($eauth);
                    // successful authentication
                    if ($identity->authenticate()) {
                        Yii::app()->user->login($identity);
                        //var_dump($identity->id, $identity->name, Yii::app()->user->id);exit;

                        // special redirect with closing popup window
                        $this->lastVisit();
                        $this->setState();
                        $eauth->redirect();
                    }else {
                        switch ($identity->errorCode){
                            case GoogleEAuthUserIdentity::ERROR_NOT_AUTHENTICATED:
                                $model->addError("email",Yii::t("web/error",'identity is incorrect'));
                                break;
                            case GoogleEAuthUserIdentity::ERROR_EMAIL_NOT_EXIST:
                                $model->addError("email",Yii::t("web/error","email not exist"));
                                break;
                            case GoogleEAuthUserIdentity::ERROR_STATUS_INACTIVE:
                                $model->addError("status",UserModule::t("You account is not activated."));
                                break;
                            case GoogleEAuthUserIdentity::ERROR_STATUS_BAN:
                                $model->addError("status",UserModule::t("You account is blocked."));
                                break;
                        }
                        // close popup window and redirect to cancelUrl
//                        $eauth->cancel();
                        $loginAttempts = Yii::app()->user->getState('login_attempts');
                        $loginAttempts++;
                        Yii::app()->user->setState('login_attempts', $loginAttempts);
                    }
                }

            }
            catch (EAuthException $e) {
                // save authentication error to session
                Yii::app()->user->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
                $eauth->redirect($eauth->getCancelUrl());
            }
        }
    }

    private function setState()
    {
        if(!Yii::app()->user->isGuest){
            $user = User::model()->notsafe()->findByPk(Yii::app()->user->id);
            if($user){
                Yii::app()->user->setState('store_id', $user->store_id);
            }
        }
    }


}