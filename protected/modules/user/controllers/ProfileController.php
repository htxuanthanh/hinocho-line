<?php

class ProfileController extends SController
{
	public $defaultAction = 'profile';
	public $layout='//layouts/column2';

	public function filters()
    {
        return array(
            'rights'
        );
    }

    /**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;
	/**
	 * Shows a particular model.
	 */
	public function actionProfile()
	{
		$model = $this->loadUser();

	    $this->render('profile',array(
	    	'model'=>$model,
			'profile'=>$model->profile,
	    ));
	}


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionEdit()
	{
		$model = $this->loadUser();
		$profile=$model->profile;
        if (!$profile) {
            $profile = new Profile();
        }
        $model->scenario = 'edit_profile';

//        CVarDumper::dump($model,10,true);die();

		// ajax validator
		if(isset($_POST['ajax']) && $_POST['ajax']==='profile-form')
		{
			echo UActiveForm::validate($model);
			Yii::app()->end();
		}

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
//			$profile->attributes=$_POST['Profile'];

			if($model->validate()) {
				$model->save();
				Yii::app()->user->setFlash('profileMessage',UserModule::t("Changes is saved."));
				$this->redirect(array('/user/profile'));
			}
		}

		$this->render('edit',array(
			'model'=>$model,
			'profile'=>$profile,
		));
	}

    public function actionUpload()
    {
        $return = array(
            'success' => FALSE,
            'error' => FALSE,
            'data' => array(
                'url' => ''
            ),
        );
        $model = new User();
        $attribute = $_POST['attribute'];
        if($file = CUploadedFile::getInstance($model, $attribute)){
            //validate
            $file_name = date('YmdHis') . rand(1000,9999) . $file->name;
            if($attribute == 'avatar'){
                $accept_ext = array('png', 'jpg', 'jpeg', 'gif');
                if(!in_array($file->extensionName, $accept_ext)){
                    $model->addError($attribute, Yii::t('web/form', 'File extension not allow. Accept only {ext}',array('{ext}' => implode(',',$accept_ext))));
                }else if($file->size > MAX_UPLOAD_IMAGE_SIZE){
                    $model->addError($attribute, Yii::t('web/form', 'File size too large. Max is {size}', array('{size}' => '2MB')));
                }else if(strlen($file_name) > MAX_UPLOAD_FILE_NAME_SIZE){
                    $model->addError($attribute, Yii::t('web/form', 'File name size too large. Max is {size}', array('{size}' => MAX_UPLOAD_FILE_NAME_SIZE)));
                }
            }
            //upload
            if(!$model->hasErrors()){
                $DS = DIRECTORY_SEPARATOR;
                $folder = 'user';
                $upload_dir = realpath(Yii::app()->getBasePath() . $DS . '..') . $DS . Yii::app()->params->upload_dir . $DS . $folder;
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir, 0777, TRUE);
                }

                if ($file->saveAs($upload_dir . $DS .  $file_name)) {
                    $return['success'] = TRUE;
                    $return['data']['url'] = Yii::app()->params->upload_dir . $DS . $folder . $DS . $file_name;
                }else{
                    $model->addError($attribute, Yii::t('web/form', 'upload file failed'));
                }
            }
        }else{
            $model->addError($attribute, Yii::t('web/form', 'empty file upload'));
        }

        if($model->hasErrors()){
            $return['error'] = $model->getErrors();
        }
        echo CJSON::encode($return);
        Yii::app()->end();
    }

	/**
	 * Change password
	 */
	public function actionChangepassword() {
		$model = new UserChangePassword;
		if (Yii::app()->user->id) {

			// ajax validator
			if(isset($_POST['ajax']) && $_POST['ajax']==='changepassword-form')
			{
				echo UActiveForm::validate($model);
				Yii::app()->end();
			}

			if(isset($_POST['UserChangePassword'])) {
					$model->attributes=$_POST['UserChangePassword'];
					if($model->validate()) {
						iF(User::model()->updateByPk(Yii::app()->user->id,
						    [
                                'password' => UserModule::encrypting($model->password),
                                'activkey' => UserModule::encrypting(microtime().$model->password)
                            ]
                        )){
                            Yii::app()->user->setFlash('profileMessage',UserModule::t("New password is saved."));
                            $this->redirect(array("profile"));
                        };
					}
			}
			$this->render('changepassword',array('model'=>$model));
	    }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the primary key value. Defaults to null, meaning using the 'id' GET variable
	 */
	public function loadUser()
	{
		if($this->_model===null)
		{
			if(Yii::app()->user->id)
				$this->_model=Yii::app()->controller->module->user();
			if($this->_model===null)
				$this->redirect(Yii::app()->controller->module->loginUrl);
		}
		return $this->_model;
	}
}