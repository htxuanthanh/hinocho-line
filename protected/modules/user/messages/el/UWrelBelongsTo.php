<?php

return array(
	'Model Name'=>'Ονομασία Μοντέλου',
	'Label field name'=>'Όνομα πεδίου ετικέτας',
	'Empty item name'=>'Όνομα κενού αντικειμένου',
	'Profile model relation name'=>'Όνομα  σχέσης του Προφίλ Μοντέλου.',
);
