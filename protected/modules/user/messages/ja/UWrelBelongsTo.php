<?php

return array(
	'Model Name'=>'モデル名',
	'Label field name'=>'ラベル・フィールド名',
	'Empty item name'=>'空白アイテム名',
	'Profile model relation name'=>'プロフィール・モデル・リレーション名',
);
