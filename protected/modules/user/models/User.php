<?php

/**
 * The followings are the available columns in table 'users':
 * @property  integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $activkey
 * @property integer $superuser
 * @property integer $status
 * @property string $create_at
 * @property string $lastvisit_at
 *
 * @property string $phone
 * @property string $name
 * @property string $avatar
 * @property string $store_id
 */
class User extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANNED=-1;

	public $re_password;
	public $roles;

	public $store_search_key;

    /**
	 * Returns the static model of the specified AR class.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->getModule('user')->tableUsers;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.CConsoleApplication
		return array(
            array('username, email, status, name, phone', 'required'),
            array('username', 'length', 'max' => 20, 'min' => 3, 'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
            array('password', 'length', 'max' => 128, 'min' => 4, 'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
            array('username', 'match', 'pattern' => '/^([A-Za-z0-9_\.-])+$/', 'message' => Yii::t('web/error','username invalid pattern')),
            array('password, re_password', 'required', 'on' => 'insert'),
            array('re_password', 'length', 'max' => 128, 'min' => 4, 'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
            array('re_password', 'compare', 'compareAttribute' => 'password', 'on' => 'insert', 'message' => UserModule::t('Retype Password is incorrect.')),
            array('email', 'email'),
            array('status', 'in', 'range' => array(self::STATUS_NOACTIVE, self::STATUS_ACTIVE, self::STATUS_BANNED)),
            array('superuser', 'in', 'range' => array(0, 1)),
            array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
            array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
            array('roles', 'safe', 'on' => 'search'),

            array('roles', 'required', 'on' => 'insert, update'),
            array('name, avatar, address, store_id', 'length', 'max' => 255),
            array('phone', 'length', 'max' => 20),

            //extend
            array('store_search_key', 'safe'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
        $relations = Yii::app()->getModule('user')->relations;
        if (!isset($relations['profile'])){
            $relations['profile'] = array(self::HAS_ONE, 'Profile', 'user_id');
        }
        $relations['store'] = array(self::HAS_ONE, 'WStore', array('id' => 'store_id'));
        return $relations;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => UserModule::t("ID"),
			'username'=>UserModule::t("Username"),
			'password'=>UserModule::t("Password"),
			'verifyPassword'=>UserModule::t("Retype Password"),
			'email'=>UserModule::t("E-mail"),
			'verifyCode'=>UserModule::t("Verification Code"),
			'activkey' => UserModule::t("Activation key"),
			'create_at' => UserModule::t("Registration date"),
			'lastvisit_at' => UserModule::t("Last visit"),
			'superuser' => UserModule::t("Superuser"),
			'status' => UserModule::t("Status"),

            're_password' => UserModule::t('Retype Password'),
            'roles' => Yii::t('web/model/user','roles'),
            'phone' => Yii::t('web/model/user','phone'),
            'name' => Yii::t('web/model/user','name'),
            'avatar' => Yii::t('web/model/user','avatar'),
            'store_id' => Yii::t('web/model/user','store_id'),
		);
	}

	public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'status='.self::STATUS_ACTIVE,
            ),
            'notactive'=>array(
                'condition'=>'status='.self::STATUS_NOACTIVE,
            ),
            'banned'=>array(
                'condition'=>'status='.self::STATUS_BANNED,
            ),
            'superuser'=>array(
                'condition'=>'superuser=1',
            ),
            'notsafe'=>array(

            ),
        );
    }

	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'UserStatus' => array(
				self::STATUS_NOACTIVE => 'Inactive',
				self::STATUS_ACTIVE => 'Active',
				self::STATUS_BANNED => 'Banned',
			),
			'AdminStatus' => array(
				'0' => UserModule::t('No'),
				'1' => UserModule::t('Yes'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

/**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addCondition("t.username != 'admin'");
        $criteria->with = array('store');

        $criteria->compare('t.username',$this->username,true);
        $criteria->compare('t.email',$this->email,true);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.phone',$this->phone);
        $criteria->compare('t.name', $this->name);
        if (!empty($this->store_search_key)) {
            $criteria->compare('store.name', $this->store_search_key, TRUE);
        }

        if(!empty($this->roles)){
            $criteria->addCondition("t.id IN (SELECT userid FROM authassignment WHERE itemname = '$this->roles')");
        }

        if(!empty(Yii::app()->user->store_id)){
            $criteria->compare('store_id', Yii::app()->user->store_id);
        }

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
        	'pagination'=>array(
				'pageSize'=>Yii::app()->getModule('user')->user_page_size,
			),
        ));
    }

    public static function getAvatarUrl($id)
    {
        $user = User::model()->notsafe()->findByAttributes(array('id' => $id));
        if($user && !empty($user->avatar)){
            $url = Yii::app()->baseUrl . '/' . $user->avatar;
        }else{
            $url = Yii::app()->theme->baseUrl . '/images/user.png';
        }
        return $url;
    }

    /**
     * @param $id int user->id
     * @return bool
     */
    public static function isSuperUser($id)
    {
        $isSuperUser = false;
        $user = User::model()->findByPk($id);
        if($user){
            $isSuperUser = ($user->superuser == 1);
        }
        return $isSuperUser;
    }

    /**
     * @param $id int user->id
     * @param $roles string | array
     */
    public static function isUserHasRoles($id, $roles)
    {
        if(!is_array($roles)){
            $roles = array($roles);
        }

        $criteria = new CDbCriteria();
        $criteria->compare('userid', $id, FALSE);
        $criteria->addInCondition('itemname', $roles);
        $authassignments = Authassignment::model()->count($criteria);

        return ($authassignments == count($roles));
    }

    public static function getListRoles()
    {
        return array(
            'Admin'    => Yii::t('web/model/user', 'Admin'),
        );
    }

    public static function getRolesName($role)
    {
        $data = self::getListRoles();
        return (isset($data[$role])) ? $data[$role] : $role;
    }

    public static function getListStatus()
    {
        return array(
            User::STATUS_ACTIVE   => Yii::t('web/form', 'Active'),
            User::STATUS_NOACTIVE => Yii::t('web/form', 'Inactive'),
            User::STATUS_BANNED => Yii::t('web/form', 'Banned'),
        );
    }

    protected function afterFind()
    {
        $this->roles = array();
        $criteria = new CDbCriteria();
        $criteria->compare('userid', $this->id, FALSE);
        $criteria->addInCondition('itemname', array_keys($this->getListRoles()));
        $roles = Authassignment::model()->findAll($criteria);
        foreach ($roles as $role) {
            $this->roles[] = $role->itemname;
        }

        return TRUE;
    }

    protected function beforeSave()
    {
        if($this->isNewRecord){
            $this->password=Yii::app()->controller->module->encrypting($this->password);
            $this->create_at = date('Y-m-d H:i:s');
            if(!empty(Yii::app()->user->store_id)){
                $this->store_id = Yii::app()->user->store_id;
            }
        }

        return TRUE;
    }

    protected function afterSave()
    {
        if(in_array($this->scenario, array('insert', 'update'))){
            if(!empty($this->roles)){
                $criteria = new CDbCriteria();
                $criteria->addInCondition('itemname', array_keys(User::getListRoles()));
                $criteria->compare('userid', $this->id, FALSE);
                $authassignments = Authassignment::model()->findAll($criteria);
                $old_roles = array_keys(CHtml::listData($authassignments, 'itemname', 'itemname'));

                $list_roles_delete = array_diff($old_roles, $this->roles);
                $list_roles_create = array_diff($this->roles, $old_roles);


                if(!empty($list_roles_delete)){
                    Authassignment::model()->deleteAllByAttributes(array(
                        'userid' => $this->id,
                        'itemname' => $list_roles_delete
                    ));
                }

                if(!empty($list_roles_create)){
                    foreach ($list_roles_create as $role){
                        $authassignment = new Authassignment();
                        $authassignment->userid = $this->id;
                        $authassignment->itemname = $role;
                        $authassignment->save();
                    }
                }
            }

        }
        return TRUE;
    }

    public static function getUsersByRole($role)
    {
        if(in_array($role, array_keys(self::getListRoles()))){
            $criteria = new CDbCriteria();
            $criteria->condition = "t.status != '".self::STATUS_BANNED."' AND t.id IN (SELECT user_id FROM authassignment WHERE itemname = '$role')";
            return User::model()->findAll($criteria);
        }else{
            return FALSE;
        }
    }





}