<?php

class ExcelExportController extends SController
{
    public function init()
    {
        ini_set('max_execution_time', 60*10);
        ini_set('memory_limit', '-1');
        Yii::import('ext.yexcel.Classes.PHPExcel');
        parent::init();
    }

}