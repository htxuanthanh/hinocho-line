<?php

class ApiController extends AController
{

    protected function getToken()
    {
        $token = null;
        $header = getallheaders();
        if (is_array($header)) {
            $header = array_change_key_case($header, CASE_LOWER);
        }
        if (isset($header['authorization']) && !empty($header['authorization'])) {
            $authorization = $header['authorization'];
            $arrToken = explode(' ', $authorization);
            $token = $arrToken[1];
        }
        return $token;
    }

    protected function authorization()
    {
        $return = array(
            'success' => false,
            'message' => '',
        );
        $token = $this->getToken();
        if (!empty($token)) {
            return TRUE;
        } else {
            $return['message'] = Yii::t('web/error', 'Authorization Failed!') . ' Token empty';
        }
        $httpVersion = Yii::app()->request->getHttpVersion();
        header("HTTP/$httpVersion 403 Forbidden");
        header('Content-Type: application/json');
        $response = CJSON::encode($return);
        echo $response;
        Yii::app()->end();
    }

    public function actionMessage()
    {
        $data = file_get_contents('php://input');
        if (!empty($data) && !empty($_REQUEST['s'])) {
            $storeId = $_REQUEST['s'];
            $store = WStore::model()->findByPk($storeId);

            $messageReceiveList = json_decode($data, true);

            //取得データ
            foreach ($messageReceiveList['events'] as $messageReceive)
            {
                $params = [
                    'access_token' => $store->line_bot_access_token,
                    'store_id' => $store->id,
                ];
                $service = new LineMessageService();
                $service->init($params);
                $messageReply = $service->makeMessageReply($messageReceive);
                $res = $service->pushMessageReply($messageReply);

                //file_put_contents('/var/www/logs/log.txt', json_encode($messageReply)."\n". json_encode($res)."\n\r", FILE_APPEND);
            }
        }

        Yii::app()->end();
    }



}