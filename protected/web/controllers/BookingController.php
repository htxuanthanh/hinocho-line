<?php

class BookingController extends WController
{

    public function actionIndex()
    {
        $this->render('index', []);
    }

    public function actionFureiru($token)
    {
        $customerInfo = Yii::app()->JWT->decode($token);
        $store = WStore::model()->findByPk($customerInfo->store_id);

        $this->render('fureiru', [
            'botLink' => LineService::getBotLinkByStore($customerInfo->store_id),
            'token' => $token,
            'store' => $store,
        ]);
    }

    public function actionTimezone($token)
    {
        $customerInfo = Yii::app()->JWT->decode($token);
        $store = WStore::model()->findByPk($customerInfo->store_id);

        $this->render('timezone', [
            'botLink' => LineService::getBotLinkByStore($customerInfo->store_id),
            'token' => $token,
            'store' => $store,
        ]);
    }

    public function actionAuthXid()
    {
        $response = [
            'success' => true,
            'redirect' => ''
        ];

        if (!empty($_POST['mode']) && !empty($_POST['token'])) {
            $mode = $_POST['mode'];
            $token = $_POST['token'];

            $customerInfo = Yii::app()->JWT->decode($token);

            $store = WStore::model()->findByPk($customerInfo->store_id);
            $customer = WCustomer::model()->findByPk($customerInfo->customer_id);

            switch ($mode) {
                case 'fureiru':
                    $XIDService = new XIDServiceFake();
                    $customerXidInfo =  $XIDService->myNumberStatus(null, null);
                    if (!empty($customerInfo)) {
                        $customer->xid_info = $customerXidInfo;
                        $customer->update();
                        $response['redirect'] = $store->fureiru_full_ver_url;
                    }
                    break;
                case 'timezone':
                    $XIDService = new XIDServiceFake();
                    $customerXidInfo =  $XIDService->myNumberStatus(null, null);
                    if (!empty($customerInfo)) {
                        $customer->xid_info = $customerXidInfo;
                        $customer->update();
                        $response['redirect'] = LineService::getBotLinkByStore($customerInfo->store_id);

                        $lineService = new LineMessageService();
                        $lineService->init([
                            'access_token' => $store->line_bot_access_token,
                            'store_id' => $store->id,
                        ]);
                        $message = $lineService->makeMessageBookingTimezone($store, $customer);
                        $res = $lineService->pushMessage($message);
                    }
                    break;
            }

            echo json_encode($response);
            Yii::app()->end();
        }

        $this->ajaxResponseError();
    }

    private function ajaxResponseError($message = '')
    {
        $response = [
            'success' => false,
            'message' => $message
        ];
        echo json_encode($response);
        Yii::app()->end();
    }

}