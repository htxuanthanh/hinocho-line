<?php

class SiteController extends SController
{

    public function actionIndex()
    {
        $this->render('index', array());
    }

    public function actionError()
    {
        $this->layout = '//layouts/main_web';
        parent::actionError();
    }
}