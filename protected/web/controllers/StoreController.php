<?php

class StoreController extends SController
{

    public function actionIndex()
    {
        $model = new WStore('search');
        $model->unsetAttributes();
        if(isset($_REQUEST['WStore'])){
            $model->attributes = $_REQUEST['WStore'];
        }

        $this->render('index', array(
            'model' => $model
        ));

    }

    public function actionCreate()
    {
        $model = new WStore();
        $model->status = WStore::STATUS_ACTIVE;

        $this->performAjaxValidation($model);
        if(isset($_POST['WStore'])){
            $model->attributes = $_POST['WStore'];
            if($model->validate()){
                if($model->save()){
                    Yii::app()->user->setFlash('success', Yii::t('web/model/store', 'create model {name} success', array('{name}' => $model->name)));
                    $this->redirect(Yii::app()->createUrl('store/update', array('id' => $model->id)));
                }else{
                    Yii::app()->user->setFlash('error', Yii::t('web/model/store', 'create model {name} fail', array('{name}' => $model->name)));
                }
            }

        }
        $this->render('create', array(
            'model' => $model
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);
        if(isset($_POST['WStore'])){
            $model->attributes = $_POST['WStore'];
            if($model->validate()){
                if($model->save()){
                    Yii::app()->user->setFlash('success', Yii::t('web/model/store', 'update model {name} success', array('{name}' => $model->name)));
                    $this->redirect(Yii::app()->createUrl('store/update', array('id' => $model->id)));
                }else{
                    Yii::app()->user->setFlash('error', Yii::t('web/model/store', 'update model {name} fail', array('{name}' => $model->name)));
                }
            }

        }
        $this->render('update', array(
            'model' => $model
        ));
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        if($model){
            $model->softDelete();
        }

        if (!isset($_REQUEST['ajax'])) {
            $this->redirect(isset($_REQUEST['returnUrl']) ? $_REQUEST['returnUrl'] : array('index'));
        }
    }


    /**
     * @param $id
     * @return WStore
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = WStore::model()->findByPk($id);
        if ($model === NULL || (!empty(Yii::app()->user->store_id) && $model->id != Yii::app()->user->store_id)){
            throw new CHttpException(404, Yii::t('web/error', 'page not found'));
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param $model
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='store-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}