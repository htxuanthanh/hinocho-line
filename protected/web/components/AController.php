<?php

class AController extends CController
{
    public function init()
    {
        Yii::app()->errorHandler->errorAction='api/error';
        $this->preventXSS();
    }

    public function preventXSS()
    {
        if (isset($_GET) && count($_GET) > 0) {
            $p = new CHtmlPurifier();
            foreach ($_GET as $k => $v) {
                $_GET[$k] = $p->purify($v);
            }
        }

        if (isset($_POST) && count($_POST) > 0) {
            $p = new CHtmlPurifier();
            foreach ($_POST as $k => $v) {
                $_POST[$k] = $p->purify($v);
            }
        }
    }

    public function actionError()
    {
        $error = Yii::app()->errorHandler->error;
        echo Yii::t('web/label', 'Error Message') . ': ' . CHtml::encode($error['message']);
    }

}
