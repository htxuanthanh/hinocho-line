<?php

class HttpRequest extends CHttpRequest
{
    public $noCsrfValidationRoutes = array();

    public function validateCsrfToken($event)
    {
        if(isset($_REQUEST['YII_CSRF_TOKEN'])){
            $_POST['YII_CSRF_TOKEN'] = $_REQUEST['YII_CSRF_TOKEN'];
        }

        $url = Yii::app()->getUrlManager()->parseUrl($this);
        foreach ($this->noCsrfValidationRoutes as $route) {
            if ($url == $route) {
                return TRUE;
            }
            if (substr($route, -2) == '/*') {
                $_route = str_replace('/*', '', $route);
                if(strpos($url, $_route) === 0) {
                    return TRUE;
                }
            }
        }

        return parent::validateCsrfToken($event);
    }
}

?>