<?php

class WController extends Controller
{
    public function init()
    {
        $this->pageTitle = Yii::app()->name;
        $this->checkCookiesLanguage();
        $this->preventXSS();
        $this->layout = '//layouts/main_web';
    }

    public function filters()
    {
        return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function checkCookiesLanguage()
    {
        if(isset(Yii::app()->request->cookies['language']->value) && !empty(isset(Yii::app()->request->cookies['language']->value))){
            Yii::app()->setLanguage(Yii::app()->request->cookies['language']->value);
        }
    }

    public function preventXSS()
    {
        if (isset($_GET) && count($_GET) > 0) {
            $p = new CHtmlPurifier();
            foreach ($_GET as $k => $v) {
                $_GET[$k] = $p->purify($v);
            }
        }

        if (isset($_POST) && count($_POST) > 0) {
            $p = new CHtmlPurifier();
            foreach ($_POST as $k => $v) {
                $_POST[$k] = $p->purify($v);
            }
        }
    }

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

}
