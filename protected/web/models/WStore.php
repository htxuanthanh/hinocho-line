<?php

class WStore extends Store
{

    /**
     * @return WStore the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('status, deleted', 'numerical', 'integerOnly'=>true),
            array('name, line_bot_access_token, line_bot_basic_id, xid_client_id, xid_client_secret', 'length', 'max'=>255),
            array('xid_public_key, xid_secret_key, xid_auth_info, created_at, updated_at, deleted_at, fureiru_uncheck_ver_url, fureiru_full_ver_url, timezone_url', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, line_bot_access_token, line_bot_basic_id, xid_client_id, xid_client_secret, xid_public_key, xid_secret_key, xid_auth_info, status, created_at, updated_at, deleted_at, deleted, fureiru_uncheck_ver_url, fureiru_full_ver_url, timezone_url', 'safe', 'on'=>'search'),
            //insert update
            array('name, line_bot_access_token, line_bot_basic_id, xid_client_id, xid_client_secret, xid_public_key, xid_secret_key, fureiru_uncheck_ver_url, fureiru_full_ver_url, timezone_url', 'required', 'on' => 'insert,update'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'                    => 'ID',
            'name'                  => Yii::t('web/model/store','name'),
            'line_bot_access_token' => 'Line Bot Access Token',
            'line_bot_basic_id'     => 'Line Bot Basic',
            'xid_client_id'         => 'Xid Client',
            'xid_client_secret'     => 'Xid Client Secret',
            'xid_public_key'        => 'Xid Public Key',
            'xid_secret_key'        => 'Xid Secret Key',
            'xid_auth_info'         => 'Xid Auth Info',
            'fureiru_uncheck_ver_url'   => 'Xid Uncheck Ver Url',
            'fureiru_full_ver_url'      => 'Xid Full Ver Url',
            'timezone_url'          => 'Timezone Url',
            'status'                => Yii::t('web/model/store','status'),
            'created_at'            => Yii::t('web/label','created_at'),
            'updated_at'            => Yii::t('web/label','updated_at'),
            'deleted_at'            => Yii::t('web/label','deleted_at'),
            'deleted'               => Yii::t('web/label','deleted'),
        );
    }




    public function search($dataProvider = true)
    {
        $criteria=new CDbCriteria;

        $criteria->addCondition('t.deleted IS NULL');
        $criteria->compare('t.name',$this->name, TRUE);

        if(!empty(Yii::app()->user->store_id)){
            $criteria->compare('t.id', Yii::app()->user->store_id);
        }else{
            $criteria->compare('t.id', $this->id);
        }

        if($dataProvider){
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }else{
            return self::model()->findAll($criteria);
        }
    }

    public static function getListData()
    {
        $cache_key = __CLASS__ . '_' . __FUNCTION__;
        $data = Yii::app()->cache->get($cache_key);
        if (!$data) {
            $data = array();
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.deleted IS NULL');
            $models = self::model()->findAll($criteria);
            if (!empty($models)) {
                $data = CHtml::listData($models, 'id', 'name');
            }
            Yii::app()->cache->set($cache_key, $data, Yii::app()->params->cache_timeout['month']);
        }
        return $data;
    }

    public function isEnableEdit()
    {
        if (Yii::app()->user->isSuperuser) {
            return TRUE;
        }
        return FALSE;
    }

    public function isEnableDelete()
    {
        if (Yii::app()->user->isSuperuser) {
            return TRUE;
        }
        return FALSE;
    }
}