<?php

$web  = dirname(dirname(__FILE__));
$base = dirname($web);
Yii::setPathOfAlias('web', $web);
$commonConfig = require($base . '/config/main.php');
$webConfig = array(
    'basePath'          => $base,
    'preload'           => array(
        'log',
        'yiibooster',
    ),
    'controllerPath'    => $web . '/controllers',
    'viewPath'          => $web . '/views',
    'runtimePath'       => $web . '/runtime',
    'theme'             => 'gentelella',
    'defaultController' => 'site',
    'import'            => array(
        'web.models.*',
        'web.components.*',
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
        'ext.YiiMailer.YiiMailer',
        'application.helpers.*',
        'application.modules.user.UserModule',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.rights.*',
        'application.modules.rights.components.*',
        'application.services.*',
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.*',
        'ext.eauth.services.*',
        'ext.validators.PhoneNumberValidator',
        'ext.firebase.*',
    ),

    'modules'           => array(
        'user' => array(
            'tableUsers'          => 'users',
            'tableProfiles'       => 'profiles',
            'tableProfileFields'  => 'profiles_fields',

            # encrypting method (php hash function)
            'hash'                => 'md5',

            # send activation email
            'sendActivationMail'  => FALSE,

            # allow access for non-activated users
            'loginNotActiv'       => FALSE,

            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => FALSE,

            # automatically login from registration
            'autoLogin'           => TRUE,

            # registration path
            'registrationUrl'     => array('/user/registration'),

            # recovery password path
            'recoveryUrl'         => array('/user/recovery'),

            # login form path
            'loginUrl'            => array('/user/login'),

            # page after login
            'returnUrl'           => array('/site/index'),

            # page after logout
            'returnLogoutUrl'     => array('/user/login'),
        ),

        'rights' => array(
            'install'           => FALSE,
            'superuserName'=>'SuperAdmin', // Name of the role with super user privileges.
//                        'authenticatedName'=>'Authenticated',  // Name of the authenticated user role.
//                        'userIdColumn'=>'id', // Name of the user id column in the database.
//                        'userNameColumn'=>'username',  // Name of the user name column in the database.
            'enableBizRule'     => FALSE,  // Whether to enable authorization item business rules.
            'enableBizRuleData' => FALSE,   // Whether to enable data for business rules.
//                        'displayDescription'=>true,  // Whether to use item description instead of name.
//                        'flashSuccessKey'=>'RightsSuccess', // Key to use for setting success flash messages.
//                        'flashErrorKey'=>'RightsError', // Key to use for setting error flash messages.
//
//                        'baseUrl'=>'/rights', // Base URL for Rights. Change if module is nested.
            'layout'            => 'webroot.themes.gentelella.views.rights.layouts.main',  // Layout to use for displaying Rights.
            'appLayout'         => 'webroot.themes.gentelella.views.layouts.main', // Application layout.
//            'cssFile'           => '/themes/gentelella/css/rights.css', // Style sheet file to use for Rights.
            'debug'             => FALSE,
        ),

        // uncomment the following to enable the Gii tool
        'gii'    => array(
            'class'          => 'system.gii.GiiModule',
            'password'       => '123456',
            'ipFilters'      => array('127.0.0.1', '::1'),
//            'ipFilters'      => [],
            'generatorPaths' => array('bootstrap.gii'),
        ),
    ),

    'components'        => array(
        'errorHandler' => array(
            'errorAction' => YII_DEBUG ? null : 'site/error',
        ),
        'request'      => array(
            'class'                  => 'web.components.HttpRequest',
            'enableCsrfValidation'   => TRUE,
            'enableCookieValidation' => TRUE,
            'noCsrfValidationRoutes' => array(
                'api/*'
            ),
        ),
        'yiibooster' => array(
            'class' => 'ext.yiibooster.components.Booster',
        ),
        'JWT' => array(
            'class' => 'ext.jwt.JWT',
            'key' => $GLOBALS['config_common']['project']['jwtPrivateKey']
        ),
        'loid' => array(
            'class' => 'ext.lightopenid.loid',
        ),
        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache'.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'services' => array(
                'google_oauth' => array(
                    'class' => 'GoogleOAuthService',
                    'client_id' => $GLOBALS['config_common']['google']['client_id'],
                    'client_secret' => $GLOBALS['config_common']['google']['client_secret'],
                    'title' => 'Google (OAuth)',
                ),
            )
        ),

        'user'        => array(
            'class'          => 'RWebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => TRUE,
            'loginUrl'       => array('/user/login'),
//            'authTimeout'    => 10,
        ),
        'authManager' => array(
            'class'           => 'RDbAuthManager',
            'connectionID'    => 'db',
            'defaultRoles'    => array('Authenticated'),
            'itemTable'       => 'authitem',
            'itemChildTable'  => 'authitemchild',
            'assignmentTable' => 'authassignment',
            'rightsTable'     => 'rights',
        ),

        'urlManager' => array(
            'urlFormat'      => 'path', // 'path' or 'get'
            'showScriptName' => FALSE,  // show index.php
            'caseSensitive'  => TRUE,  // case sensitive
//            'urlSuffix'      => '.html',
            'rules'          => array(
                ''                                          => '/user/login',
                'dashboard'                                 => 'site/index',
                'error/<code:\d*>'                          => 'site/error',
                '<controller:\w+>/<action:\w+>'             => '<controller>/<action>',

            ),
        ),
        /*'urlManager'   => array(
            'urlFormat' => 'get', // 'path' or 'get'
            'rules'     => array(),
        ),*/

        'yexcel' => array(
            'class' => 'ext.yexcel.Yexcel'
        ),
    ),
    'params'            => require(dirname(__FILE__) . '/params.php'),
);

return w3_array_union_recursive($webConfig, $commonConfig);
