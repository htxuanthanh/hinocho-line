<?php

return array(
    'error' => 'Error',
    '{table} not exist' => '{table} not exist',
    'wrong datatype' => 'Wrong datatype',
    'identity is incorrect' => 'Identity incorrect',
    'email not exist' => 'Email not exist',
    'you are not allow to access this route' => 'You are not allow to access this route',
    'page not found' => 'Page not found',

    'missing data request' => 'Missing request data',
    'wrong method request' => 'Wrong request method',
    'Day session passed, Unable to access!' => 'This function enable time has expired!',
    'Authorization Failed!' => 'Authorization failed!',
    'Empty request' => 'Empty request',
    'IP blocked' => 'IP blocked',
    'invalid request' => 'Invalid request',
    'data not found' => 'Data not found',

);