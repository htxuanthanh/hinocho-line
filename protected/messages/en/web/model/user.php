<?php

return array(
    'roles' => 'Role',
    'phone' => 'Phone',
    'name' => 'Name',
    'avatar' => 'Avatar',
    'store_id' => 'Store',

    'create model {name} success'   => 'Create user {name} success',
    'create model {name} fail'      => 'Create user {name} fail',
    'update model {name} success'   => 'Update user {name} success',
    'update model {name} fail'      => 'Update user {name} fail',
);
