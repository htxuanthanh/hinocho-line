<?php

return array(

    'name'      => 'Store name',

    'create model {name} success'   => 'Create store {name} success',
    'create model {name} fail'      => 'Create store {name} fail',
    'update model {name} success'   => 'Update store {name} success',
    'update model {name} fail'      => 'Update store {name} fail',
);
