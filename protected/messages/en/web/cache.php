<?php

return array(
    'manage_cache'            => 'Cache',
    'clear_cache'             => 'You can clear Cache & Assets in Admin, Web and Wap',
    'web_assets_cleared'      => 'Web Assets has been cleared!',
    'adm_assets_cleared'      => 'Admin Assets has been cleared!',
    'wap_assets_cleared'      => 'Wap Assets has been cleared!',
    'web_cache_cleared'       => 'Web Cache has been cleared!',
    'adm_cache_cleared'       => 'Admin Cache has been cleared!',
    'wap_cache_cleared'       => 'Wap Cache has been cleared!',
    'error_wap_cache_cleared' => 'Error clear Wap Cache!',
    'error_web_cache_cleared' => 'Error clear Web Cache!',
    'error_adm_cache_cleared' => 'Error clear Admin Cache!',
    'error'                   => 'Error!',
    'cache_cleared'           => 'Cache has been cleared!',
);