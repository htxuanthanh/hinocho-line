<?php

return array(
    'manage_cache'            => 'Quản lý Cache của hệ thống',
    'clear_cache'             => 'Bạn có thể Clear Cache & Assets trong Admin, Web và Wap',
    'web_assets_cleared'      => 'Assets trong web đã được xóa!',
    'adm_assets_cleared'      => 'Assets trong admin đã được xóa!',
    'wap_assets_cleared'      => 'Assets trong wap đã được xóa!',
    'web_cache_cleared'       => 'Cache trong web đã được xóa!',
    'adm_cache_cleared'       => 'Cache trong admin đã được xóa!',
    'wap_cache_cleared'       => 'Cache trong wap đã được xóa!',
    'error_wap_cache_cleared' => 'Xảy ra lỗi khi xóa Cache trong wap!',
    'error_web_cache_cleared' => 'Xảy ra lỗi khi xóa Cache trong web!',
    'error_adm_cache_cleared' => 'Xảy ra lỗi khi xóa Cache trong admin!',
    'error'                   => 'Có lỗi xảy ra!',
);