<?php

return array(
    'roles' => 'Chức vụ',
    'phone' => 'Điện thoại',
    'name' => 'Họ tên',
    'avatar' => 'Ảnh đại diện',
    'store_id' => 'Store',

    'create model {name} success'   => 'Tạo mới tài khoản {name} thành công',
    'create model {name} fail'      => 'Tạo mới tài khoản {name} thất bại',
    'update model {name} success'   => 'Cập nhật tài khoản {name} thành công',
    'update model {name} fail'      => 'Cập nhật tài khoản {name} thất bại',
);
