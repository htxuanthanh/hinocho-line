<?php

return array(

    'name'      => 'Tên cửa hàng',

    'create model {name} success'   => 'Tạo mới cửa hàng {name} thành công',
    'create model {name} fail'      => 'Tạo mới cửa hàng {name} thất bại',
    'update model {name} success'   => 'Cập nhật cửa hàng {name} thành công',
    'update model {name} fail'      => 'Cập nhật cửa hàng {name} thất bại',
);
