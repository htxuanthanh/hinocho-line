<?php

return array(
    'error' => 'Có lỗi xảy ra',
    '{table} not exist' => '{table} không tồn tại',
    'wrong datatype' => 'Sai kiểu dữ liệu',
    'identity is incorrect' => 'Xác thực không chính xác',
    'email not exist' => 'Email chưa được đăng ký',
    'you are not allow to access this route' => 'Bạn không có quyền truy cập vào đường dẫn này',
    'page not found' => 'Không tìm thấy trang bạn yêu cầu',

    'missing data request' => 'Thiếu dữ liệu truyền vào',
    'wrong method request' => 'Sai kiểu truy vấn',
    'Day session passed, Unable to access!' => 'Chức năng này đã quá thời gian sử dụng!',
    'Authorization Failed!' => 'Xác thực thông tin thất bại',
    'Empty request' => 'Không tìm thấy dữ liệu truy vấn',
    'IP blocked' => 'IP truy cập đã bị chặn',
    'invalid request' => 'Truy vấn không hợp lệ',
    'data not found' => 'Không tìm thấy dữ liệu',
    'please recheck data' => 'Vui lòng kiểm tra lại dữ liệu'

);