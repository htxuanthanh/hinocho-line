<?php

/**
 * This is the model class for table "store".
 *
 * The followings are the available columns in table 'store':
 * @property integer $id
 * @property string $name
 * @property string $line_bot_access_token
 * @property string $line_bot_basic_id
 * @property string $xid_client_id
 * @property string $xid_client_secret
 * @property string $xid_public_key
 * @property string $xid_secret_key
 * @property string $xid_auth_info
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $deleted
 * @property string $fureiru_uncheck_ver_url
 * @property string $fureiru_full_ver_url
 * @property string $timezone_url
 */
class Store extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'store';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, deleted', 'numerical', 'integerOnly'=>true),
			array('name, line_bot_access_token, line_bot_basic_id, xid_client_id, xid_client_secret', 'length', 'max'=>255),
			array('xid_public_key, xid_secret_key, xid_auth_info, created_at, updated_at, deleted_at, fureiru_uncheck_ver_url, fureiru_full_ver_url, timezone_url', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, line_bot_access_token, line_bot_basic_id, xid_client_id, xid_client_secret, xid_public_key, xid_secret_key, xid_auth_info, status, created_at, updated_at, deleted_at, deleted, fureiru_uncheck_ver_url, fureiru_full_ver_url, timezone_url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'line_bot_access_token' => 'Line Bot Access Token',
			'line_bot_basic_id' => 'Line Bot Basic',
			'xid_client_id' => 'Xid Client',
			'xid_client_secret' => 'Xid Client Secret',
			'xid_public_key' => 'Xid Public Key',
			'xid_secret_key' => 'Xid Secret Key',
			'xid_auth_info' => 'Xid Auth Info',
			'status' => 'Status',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
			'deleted' => 'Deleted',
			'fureiru_uncheck_ver_url' => 'Xid Uncheck Ver Url',
			'fureiru_full_ver_url' => 'Xid Full Ver Url',
			'timezone_url' => 'Timezone Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('line_bot_access_token',$this->line_bot_access_token,true);
		$criteria->compare('line_bot_basic_id',$this->line_bot_basic_id,true);
		$criteria->compare('xid_client_id',$this->xid_client_id,true);
		$criteria->compare('xid_client_secret',$this->xid_client_secret,true);
		$criteria->compare('xid_public_key',$this->xid_public_key,true);
		$criteria->compare('xid_secret_key',$this->xid_secret_key,true);
		$criteria->compare('xid_auth_info',$this->xid_auth_info,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);
		$criteria->compare('deleted',$this->deleted);
		$criteria->compare('fureiru_uncheck_ver_url',$this->fureiru_uncheck_ver_url,true);
		$criteria->compare('fureiru_full_ver_url',$this->fureiru_full_ver_url,true);
		$criteria->compare('timezone_url',$this->timezone_url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Store the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
