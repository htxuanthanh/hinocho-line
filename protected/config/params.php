<?php

return array(
    'adminEmail' => array(
        'host'     => 'smtp.gmail.com',
        'username' => '',
        'password' => '',
        'port'     => 465,
        'secure'   => 'ssl',
    ),
    'page_size'=>'20',
    'hotline' => '0123456789',
    'upload_dir' => 'uploads',
    'cache_timeout' => array(
        'month' => 60*60*24*30,
        'day' => 60*60*24,
        'hour' => 60*60,
    ),
);