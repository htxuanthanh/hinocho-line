<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>$GLOBALS['config_common']['project']['name'],
    'language'=> $GLOBALS['config_common']['project']['language'],
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'application.web.models.*',
        'application.web.components.*',
    ),

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(

        'db'=> array(
            'connectionString'      => $GLOBALS['config_common']['db']['connectionString'],
            'username'              => $GLOBALS['config_common']['db']['username'],
            'password'              => $GLOBALS['config_common']['db']['password'],
            'tablePrefix'           => $GLOBALS['config_common']['db']['tablePrefix'],
            'emulatePrepare'        => TRUE,
            'enableProfiling'       => TRUE,
            'enableParamLogging'    => TRUE,
            'charset'               => 'utf8',
            'schemaCachingDuration' => '3600',
        ),

        'cache'=>array(
            'class' => 'CFileCache'
        ),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error', //, warning
				),
			),
		),

	),

    'params'=>require(dirname(__FILE__).'/params.php'),
);
