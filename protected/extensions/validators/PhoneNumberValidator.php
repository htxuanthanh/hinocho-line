<?php

class PhoneNumberValidator extends CValidator
{
    protected function validateAttribute($object, $attribute)
    {
        $regex = '/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/';
        if(!empty($object->$attribute) && !preg_match($regex, $object->$attribute)){
            $message = $this->message !== null ? $this->message : Yii::t("PhoneNumberValidator", "{attribute} invalid");
            $this->addError($object, $attribute, $message);
        }
    }
}
