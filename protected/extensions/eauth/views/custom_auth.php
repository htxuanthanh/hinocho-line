<div class="services">
    <ul class="auth-services clear">
        <?php
        foreach ($services as $name => $service) {
            echo '<li class="auth-service ' . $service->id . '">';
            $html = '<span class="auth-icon ' . $service->id . '"></span>';
            $html .= '<span class="auth-title">' . $service->title . '</span>';
            $html = CHtml::link($html, array($action, 'service' => $name), array(
                'class' => 'auth-link ' . $service->id . ' ' . $service->htmlOptions['class'],
            ));
            echo $html;
            echo '</li>';
        }
        ?>
    </ul>
</div>

<style>
    .auth-services{
        padding: 0;
        margin: 0;
    }

    .auth-service{
        float: none;
        margin: 0 auto;
        display: inline-block;
        text-align: center;
    }
    .auth-services .auth-service .auth-link{
        width: 100%;
        display: inline-block;
        margin: 0;
        padding: 1px 5px 1px 1px;
    }

    .auth-services .auth-service .auth-link .auth-icon,
    .auth-services .auth-service .auth-link .auth-title{
        float: left;
        display: inline-block;
    }

    .auth-service .auth-link .auth-title{
        margin-top: 0.7em;
        margin-left: 5px;
    }

    .auth-icon.google_oauth{
        background: url("<?php echo Yii::app()->theme->baseUrl?>/images/google_logo.png") no-repeat;
        background-size: 32px 32px;
        margin-right: 8px;
    }

</style>