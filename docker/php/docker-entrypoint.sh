#!/usr/bin/env bash
mkdir -p /var/www/html/tmp; \
    chmod -R 777 /var/www/html/tmp; \
    mkdir -p /var/www/lib/smarty/tmpl_c; \
    chmod -R 777 /var/www/lib/smarty/tmpl_c; \
    mkdir -p /var/www/lib/smarty/cache; \
    chmod -R 777 /var/www/lib/smarty/cache
